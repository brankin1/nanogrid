# N A N O G R I D
# definition of the base class of Nanogrid objects
# modified to work on Python3, January, 2015 [ changes to print, thread and xmlrpc ]
# Copyright: Donal O'Mahony, Trinity College, 2012, 2013

from tkinter import *
import _thread
import sys
import time
import socket
# Use XML Remote Procedure Calls to Communicate between Nanogrids
from xmlrpc.server import SimpleXMLRPCServer
import xmlrpc.client
import datetime
import configparser
import xmlrpc
from threading import Thread
from appliance import Appliance
#from broker import Broker
import configparser
import threading
import argparse

join = ""
config_name = ""
parser = argparse.ArgumentParser()
parser.add_argument("app_name")
parser.add_argument("config_name")
parser.add_argument("join")

try:
    args = parser.parse_args()
    app_name = args.app_name
    config_name = args.config_name
    join = args.join
except:
    app_name = "app1"
    config_name = "config.cfg"
try:
    join = args.join
    print(app_name + " will join the broker automatically.")
except:
    print("No join argument. Join " + app_name + " manually.")

print("Starting " + app_name + " : " + config_name + " : " + join)
cf = configparser.ConfigParser()
cf.read(config_name)
    
class ApplianceConfig:
    earliest_hour = int(cf[app_name]['start_time'].split(".")[0])
    earliest_minute = int(cf[app_name]['start_time'].split(".")[1])
    duration_hour = int(cf[app_name]['duration'].split(".")[0])
    duration_minute =  int(cf[app_name]['duration'].split(".")[1])
    max_price = cf[app_name]['max_price']
    energy_required = int(cf[app_name]['energy_required'])
    max_load = int(cf[app_name]['max_load'])
    reallocate_amount = int(cf[app_name]['reallocate_percentage'])/100
    dutch_auction = False
    tatonnement = False
    price_by_capacity = False

    auction_type = cf['default']['auction_type']
    if auction_type == "tatonnement":
        tatonnement = True
    elif auction_type == "price_by_capacity":
        price_by_capacity = True

REMOTE_RPC_HOST = cf['server']['host']
REMOTE_RPC_PORT = cf['server']['port']
broker = xmlrpc.client.ServerProxy('http://'+REMOTE_RPC_HOST+":"+str(REMOTE_RPC_PORT), allow_none=True)
app_config = ApplianceConfig()
my_appliance = Appliance(broker, app_config)

class ApplianceThread(threading.Thread):
    def __init__(self, appliance_config=None):
        super(ApplianceThread, self).__init__()
        self.broker = broker
        self.id = "Join for ID"
        self.bid_session_id = "Join for session ID"
        self.bid_status = "Data of bid in current TS"
        # The data we are bidding, containing the energy usage that we wish to receive for each time slot.
        self.bid_time_slots = None
        # List of TimeSlot. The known data that has been accepted by the server.
        self.accepted_time_slots = None
        self.total_energy_consumed = None
        self.total_price = StringVar()

        self.last_accepted_timeslots = StringVar()
        self.last_accepted_bid = StringVar()
        self.accepted_bid_string = "Test"
        updated_data = ""

    def run(self):
        while True:
            try:
                self.id = my_appliance._id
                self.accepted_time_slots = my_appliance._accepted_time_slots
                self.bid_time_slots = my_appliance._bid_time_slots
                # Display the information about the last accepted bid
                ats_string = "Start time \t\t Energy allocated \t\t Price\n"
                for ats in my_appliance._accepted_time_slots:
                    ats_string += str(ats.start_time) + "\t" + str(round(ats.energy,2)) + "\t\t\t" + str(round(ats.price,2)) + "\n"
                    current_price += round(ats.price,2) * round(ats.energy,2)
                self.last_accepted_timeslots.set(ats_string)

                self.total_price.set(current_price)
                print("ats_string: " + ats_string)

            except:
                pass
            time.sleep(.2)

class Nanogrid:
    
# Each Nanogrid has a Heartbeat function which will be called (by the Tkinter Root Window
# at periodic intervals - it can look after monitoring devices, changes in the environment etc
# In this example, it decrements the power_level variable and then makes sure that this
# new valule gets displayer by Tkinter

    def heartbeat(self):
        global displayed_id, displayed_bid_session, displayed_bid_data, displayed_price
        displayed_id.set(my_appliance._id)
        displayed_bid_session.set(my_appliance._bid_id)
        displayed_bid_data.set(self.update_bid_status())
        displayed_price.set(self.update_price())

        self.master.after(1000, self.heartbeat)  # after 100 msec, call heartbeat again
        
    def update_bid_status(self):
        ats_string = "Time\t\tEnergy allocated\t\tPrice\n"
        for i in range(len(my_appliance.energy_requested)):
            ats_string += str(my_appliance._accepted_time_slots[i].start_time)[9:] + "\t\t" + \
                str(round(my_appliance.energy_requested[i], 2)) + "\t\t\t" + \
                str(round(my_appliance._accepted_time_slots[i].price, 2)) + "\n"
        return ats_string

    def update_price(self):
        price = 0
        for i in range(len(my_appliance.energy_requested)):
            price += round(my_appliance.energy_requested[i], 2) * round(my_appliance._accepted_time_slots[i].price, 2)
        return str(round(price,2))


    def OnButtonClick(self):
        self.labelVariable.set( self.entryVariable.get()+" (You clicked the button)" )
        self.entry.focus_set()
        self.entry.selection_range(0, END)
        
    def OnPressEnter(self,event):
        self.labelVariable.set( self.entryVariable.get()+" (You pressed ENTER)" )
        self.entry.focus_set()
        self.entry.selection_range(0, END)        

    def __init__(self, master=None):
        global power_level, displayed_power_level, displayed_id, displayed_bid_session, displayed_bid_data, displayed_price
        self.ok = 1      #used to shutdown the background threads when set to 0
        self.master = master
        self.labelVariable = StringVar()

        global appliance_thread
        appliance_thread = ApplianceThread()
        appliance_thread.setDaemon(True)
        appliance_thread.start()


        # start the thread that listens for incoming requests from other Nanogrids
        #_thread.start_new_thread(self.listener,())
        
        # put some graphical elements, two buttons and a displayed power level on window
        self.frame = Frame(master)
        self.frame.grid()
        
        #COLUMN 1
        #Print the decription of each of the menu items
        HOST = cf['server']['host']
        PORT = int(cf['server']['port'])
        menu_description = ['Total energy required: '+cf[app_name]['energy_required'],\
            'Max load: '+cf[app_name]['max_load'],\
            'Earliest start time: '+cf[app_name]['start_time'],\
            'End time: '+ str(float(cf[app_name]['start_time'])+ float(cf[app_name]['duration'])) ,\
            'Max price: ' + cf[app_name]['max_price']]
        r = 0
        for option in menu_description:
            Label(text=option, relief=RIDGE,width=24).grid(row=r,column=1,sticky='EW')
            #Entry(bg=c, relief=SUNKEN,width=10).grid(row=r,column=1)
            r = r + 1

        # Display the price for the current bid.
        total_price_outline = StringVar()
        total_price_outline.set("Total cost of upcoming times:")
        total_price_label = Label(master, textvariable=total_price_outline)
        total_price_label.grid(column=1,row=5)
        displayed_price = StringVar()
        displayed_price.set(appliance_thread.total_price)
        price_label = Label(master,textvariable=displayed_price)
        price_label.grid(column=1, row=6)

                
        displayed_power_level=StringVar()
        power_level =99
        displayed_power_level.set(str(power_level))

        # Appliance ID
        id_label=StringVar()
        id_label.set("Appliance ID:")
        Label(master, textvariable=id_label).grid(column=3,row=0,sticky='EW')
        # Display the live data from the appliance.
        displayed_id=StringVar()
        displayed_id.set(appliance_thread.id)
        Label(master, textvariable=displayed_id).grid(column=3,row=1,sticky='EW')
        # Bid ID
        bid_label=StringVar()
        bid_label.set("Current bidding session ID:")
        Label(master, textvariable=bid_label).grid(column=3,row=2,sticky='EW')

        # Display the live data from the appliance.
        displayed_bid_session=StringVar()
        displayed_bid_session.set(appliance_thread.bid_session_id)
        Label(master, textvariable=displayed_bid_session).grid(column=3,row=3,sticky='EW')

        self.join_button = Button(master, text="Join", command=self.join_broker)
        self.join_button.grid(column=3,row=4,sticky='EW')
        
        self.button = Button(master, text="QUIT", fg="red", command=self.shutdown)
        self.button.grid(column=3,row=5,sticky='EW')

        # Last accepted data
        id_label=StringVar()
        id_label.set("Last successful bid details:")
        Label(master, textvariable=id_label).grid(column=3,row=6,sticky='EW')        

        # Display the bid status.
        displayed_bid_data = StringVar()
        displayed_bid_data.set(appliance_thread.bid_status)
        Label(master, textvariable=displayed_bid_data).grid(column=3,row=7,sticky='EW')

        # Display the live data from the appliance.
        self.last_accepted_timeslots = StringVar()
        self.last_accepted_timeslots.set(appliance_thread.last_accepted_timeslots)
        data_label = Label(master,textvariable=appliance_thread.last_accepted_timeslots)
        data_label.grid(column=3,row=8,sticky='EW')

        # Kick an appliance from the command line without the need for joining
        if join == "join":
            self.join_broker()
        self.heartbeat()  # start the heartbeat function for the first time
       
    def shutdown(self):
        self.ok = 0
        self.master.after(100, root.destroy)
        
# method responding to the button push
    def say_hi(self):
        print ("hi there, everyone!")

    def get_time(self):
        #some sort of 'join' type message should be sent here to another client
        timenow = nano.get_time()
        # convert the ISO8601 string to a datetime object
        #converted = datetime.datetime.strptime(today.value, "%Y%m%dT%H:%M:%S")
        print("Time now: "  + timenow.value)

    def join_broker(self):
        print("pressed join")
        my_appliance.run()


    def bid(self):
        app = Appliance(self.energyRequired.get(), self.maxLoadVar.get(), self.earliestStartVar.get(), self.latestStartVar.get(), self.loadType.get(), self.contiguous.get())
        print(my_appliance.max_load)
        pass

# mainline
#Create a Nanogrid object and kick it off into its eventloop

if __name__ == "__main__":

    root = Tk()
    root.title("Appliance: " + app_name)
    app = Nanogrid(root)
    root.mainloop()

    while True:
        pass