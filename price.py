#Python script for getting the current Wholesale Electricity Price from the SEM-O website
import os
import time
dataFile = '/project/nanogrid/data.txt'
#os.system("rm " + dataFile)
os.system('wget "http://semorep.sem-o.com/SEMOWebSite/Default.aspx?qpReportServer=http://websemoreport/ReportServer_GOTHAMCITY/&qpReportURL=/SEMO%20Market%20Dashboard%20Reports/SMP%20and%20Load%20Table&prm_GetRunType=EA&prm_GetCurrency=EUR&rpt_Export=1" -O ~/data.txt')
electricityPrices=[]
exit=0
cDate=time.strftime("%d/%m")
cTime=time.strftime("%H:%M")
cH=int(cTime[0:2],10)
cM=int(cTime[3:5],10)
if(cM>=30):
    cM=30
else:
    cM=0
f =open(dataFile, 'r')
s = f.read()
i=s.find(cDate)
i=s.find(cDate,i)
for x in range(380):
    time=""  
    price=""
    i=s.find('align:top;">',i)
    j=s.find("<",i)
    t=s[i+12:j]
    i=j+1
    if(x>=13 and (x-13)%8==0):
        time=t
        tH=int(time[0:2],10)
        tM=int(time[3:5],10)
        if(tM==cM and tH==cH):
            exit=1
    if(x>=14 and (x-14)%8==0):
        price=t
        if(exit==1):
            electricityPrices.append(price)
            for y in range(len(electricityPrices)):
                print("Current prices are "+electricityPrices[y])  
        print("")         
