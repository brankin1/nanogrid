# N A N O G R I D
# definition of the base class of Nanogrid objects
# modified to work on Python3, January, 2015 [ changes to print, thread and xmlrpc ]
# Copyright: Donal O'Mahony, Trinity College, 2012, 2013

from tkinter import *
import _thread
import sys
import socket
from time import gmtime, strftime
import time
import threading
import xmlrpc.client
import datetime
import configparser
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
from broker import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("config_name")
cf = configparser.ConfigParser()
try:
    args = parser.parse_args()
    config_name = args.config_name
    print("Got config " + config_name)
except:
    config_name = "config.cfg"
    print("No config in command line... Running: " + config_name)

class BrokerConfig:
    log_appliances_file = "C:\\Users\\James\\Documents\\COLLEGE\\nanogrid\\logs\\" + \
        str(datetime.datetime.now()).replace(':', '').replace('.', '-')+ " appliances" ".csv"
    log_timeslots_file = "C:\\Users\\James\\Documents\\COLLEGE\\nanogrid\\logs\\" + \
        str(datetime.datetime.now()).replace(':', '').replace('.', '-')+ " timeslots" ".csv"

    cf.read(config_name,encoding='utf-8')
    auction_type = cf['default']['auction_type']

    HOST = cf['server']['host']
    PORT = int(cf['server']['port'])
    increase_price_percentage = int(cf['server']['increase_price_percentage']) /100
    decrease_price_percentage = int(cf['server']['decrease_price_percentage']) /100
    inertia_percentage = 1 - int(cf['server']['inertia_percentage']) /100
    inertia_boundary = 1 - (int(cf['server']['inertia_percentage']) + int(cf['server']['inertia_boundary'])) /100

    tatonnement = False
    price_by_capacity = False
    dutch_auction = False
    price_by_average = False
    if auction_type == "tatonnement":
        tatonnement = True
    elif auction_type == "price_by_capacity":
        price_by_capacity = True
    elif auction_type == "dutch_auction":
        dutch_auction = True
    elif auction_type == "price_by_average":
        price_by_average = True

    capacity_list = cf['server']['capacity_list']
    cl = capacity_list.split(',')
    capacity_list = []
    for cap in cl:
        if price_by_capacity == True:
            capacity_list.append(int(cap))
        else:
            capacity_list.append("-")

    # This will set the size of the windows, ie each time slot is 2 minutes
    time_slot_window_size = int(cf['default']['window_size'])
    hours_into_future = float(cf['default']['hours_into_future'])
    
    # This is the amount of windows that will be created for each hour
    time_slot_window_length = datetime.timedelta(0, 60*60*hours_into_future)
    initial_price = int(cf['server']['initial_price'])
    min_price = int(cf['server']['min_price'])
    #adjust_price_percentage = int(cf['server']['adjust_price_percentage']) /100
    increase_price_percentage = int(cf['server']['increase_price_percentage']) /100
    decrease_price_percentage = int(cf['server']['decrease_price_percentage']) /100
    inertia_percentage = 1 - int(cf['server']['inertia_percentage']) /100
    inertia_boundary = 1 - (int(cf['server']['inertia_percentage']) + int(cf['server']['inertia_boundary'])) /100
    equilibrium_boundary = (int(cf['server']['equilibrium_boundary'])) / 100

    # Create the pricing tiers for when adjusting by capacity
    increase_price_tiers = cf['tiered_pricing']['increase_tiers'].split(',')
    decrease_price_tiers = cf['tiered_pricing']['decrease_tiers'].split(',')
    pricing_tiers = []
    for tier in increase_price_tiers:
        pricing_tier = PriceTier(tier.split(':')[0], tier.split(':')[1], "increase")
        pricing_tiers.append(pricing_tier)
    for tier in decrease_price_tiers:
        pricing_tier = PriceTier(tier.split(':')[0], tier.split(':')[1], "decrease")
        pricing_tiers.append(pricing_tier)

nodelist = []
broker_config = BrokerConfig()
my_broker = Broker(broker_config)

class Constants:
    cf = configparser.ConfigParser()
    cf.read(config_name)
    HOST = cf['server']['host']
    PORT = int(cf['server']['port'])


# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

class BrokerThread(threading.Thread):
    def __init__(self):
        super(BrokerThread, self).__init__()
        self.time_slots = [StringVar()] * len(my_broker._time_slots)
        self.display_price = [StringVar()] * len(my_broker._time_slots)
        self.display_time = [StringVar()] * len(my_broker._time_slots)
        self.labels = [Label()] * len(my_broker._time_slots)
        self.appliance_energies = [0] * len(my_broker._time_slots)
        self.broker_data = StringVar()
        self.appliance_first_bid_data = StringVar()
        self.appliance_ids = [StringVar()] * len(my_broker._appliances)
        self.last_round_of_timeslots = None
        self.display = "Overview"
        self.broker_time = my_broker.clock
        updated_data = ""
        # This is the text box that displays how the prices etc are fluctuating.
        self.coloured_text = Text(width=80, height=24, wrap='none')
        self.coloured_text.tag_configure('green', foreground='#2eb513')
        self.coloured_text.tag_configure('red', foreground='#e52020')
        self.coloured_text.tag_configure('blue', foreground='#125EAB')
        self.coloured_text.tag_configure('expired', foreground='#EA7824')

    def get_first_bid(self,appliance_id):
        for bid_and_price in my_broker.bid_history:
            if bid_and_price[0].appliance_id == appliance_id:
                return bid_and_price

    def update_text_box(self):
        pass

    def run(self):
        while True:
            # The information for each appliance is different.
            if self.display == "Overview":
                first_bid_data = ""
                updated_data = str(self.display)+"\n\tTime \t\t Price \t\t Energy requested\n"
                try:
                    print("Previous session: " + str(my_broker._bidding_session_id - 1))
                    if my_broker._bidding_session_id >= 2:
                        self.last_round_of_timeslots = my_broker.session_history[my_broker._bidding_session_id - 1]
                except:
                    print("Previous session: " + str(my_broker._bidding_session_id - 1))
                    print("No value for previous session in dict yet.")

                # Set the initial values
                self.coloured_text.delete(1.0, END)
                #print("Clear")
                self.coloured_text.insert(INSERT, str(self.display) + "\nBidding session :\t" + str(my_broker._bidding_session_id) +"\n", 'blue')
                self.coloured_text.insert(INSERT, "Time now:\t" + str(my_broker.clock.time())+"\n\n", 'red')
                self.coloured_text.insert(INSERT, "Time \t\tPrice \t\tDemand\t\tCapacity\n")
                # print the coloured text in to the text box here...
                if self.last_round_of_timeslots != None and self.last_round_of_timeslots != []:

                    for time_slot in my_broker._time_slots:
                        # Get the appliance energy for each time slot.
                        appliance_energy = 0
                        appliance_energy = sum(time_slot.appliance_energies.values())
                        # Generate the text to display.
                        each_slot_string = str(time_slot.start_time)[11:] + "\t\t" + \
                                str(round(time_slot.price, 2))+ "\t\t"+ \
                                str(round(appliance_energy,2)) + "\n"

                        if time_slot.start_time < my_broker.clock:
                            # Start by inserting the start time
                            self.coloured_text.insert(INSERT, str(time_slot.start_time)[11:] + "\t\t", 'expired')
                        else:
                            self.coloured_text.insert(INSERT, str(time_slot.start_time)[11:] + "\t\t")
                        # Insert the price
                        price = str(round(time_slot.price, 2))+"\t\t"                        
                        time_slot_already_in_list = False
                        
                        # Another loop for checking stuff.
                        for last_round_time_slot in self.last_round_of_timeslots:
                            # Print the prices, and change their colours based on the previous bid.
                            if last_round_time_slot.start_time == time_slot.start_time:
                                time_slot_already_in_list = True
                                if time_slot.price > last_round_time_slot.price:
                                    self.coloured_text.insert(INSERT,price, 'green')
                                elif time_slot.price < last_round_time_slot.price:
                                    self.coloured_text.insert(INSERT,price, 'red')
                                else:
                                    self.coloured_text.insert(INSERT,price)
                                # Insert the energy demand
                                energy_demand = str(round(appliance_energy,2))
                                if sum(time_slot.appliance_energies.values()) > sum(last_round_time_slot.appliance_energies.values()):
                                    #print("Red Energy")
                                    self.coloured_text.insert(INSERT,energy_demand, 'green')
                                elif sum(time_slot.appliance_energies.values()) < sum(last_round_time_slot.appliance_energies.values()):
                                    #print("Green Energy")
                                    self.coloured_text.insert(INSERT,energy_demand, 'red')
                                else:
                                    #print("Black energy")
                                    self.coloured_text.insert(INSERT,energy_demand)
                                # Insert the capacity
                                capacity = "\t\t"+ str(time_slot.capacity_available)+'\n'
                                self.coloured_text.insert(INSERT,capacity)
                        if time_slot_already_in_list == False:
                            print("Time slot not found in list?" + str(time_slot.start_time))
                            each_slot_string = str(round(time_slot.price, 2))+ "\t\t"+ \
                                str(round(appliance_energy,2)) + "\t\t"+\
                                str(time_slot.capacity_available) + "\n"
                            self.coloured_text.insert(INSERT,each_slot_string)
                else:
                    for i in range(len(my_broker._time_slots)):
                        appliance_energy = 0
                        appliance_energy = sum(my_broker._time_slots[i].appliance_energies.values())
                        each_slot_string = str(my_broker._time_slots[i].start_time)[11:] + "\t\t" + \
                                str(round(my_broker._time_slots[i].price, 2))+ "\t\t"+ \
                                str(round(appliance_energy,2)) + "\t\t"+\
                                str(my_broker._time_slots[i].capacity_available) + "\n" 
                        self.coloured_text.insert(INSERT,each_slot_string)
                        
            # Show appliance specific information here:
            else:
                updated_data = str(self.display) + "\n\tTime \t\t Price \t Energy requested\n"
                for time_slot in my_broker._time_slots:
                    appliance_energy = 0
                    try:
                        appliance_energy = time_slot.appliance_energies[self.display]
                    except:
                        pass
                    updated_data +=(str(time_slot.start_time) + " \t " +\
                        str(round(time_slot.price, 2))+ "\t"+ \
                        str(round(appliance_energy,2)) + "\n")

                first_successful_bid = self.get_first_bid(self.display)
                first_bid_data = str("First successful bid\nTime\t\t Price \t Energy requested\n")
                if first_successful_bid != None:
                    for i in range(len(first_successful_bid[0].energies)):
                        first_bid_data += str(first_successful_bid[1][i][1]) + "\t" + \
                            str(round(first_successful_bid[1][i][0],2))+"\t"+\
                            str(round(first_successful_bid[0].energies[i],2))+"\n"

            time.sleep(1)
            
            if self.broker_data != "":
                self.broker_data.set(updated_data)
                self.appliance_first_bid_data.set(first_bid_data)
            # Set the list of appliances to display in the listbox
            self.appliance_ids = ['test'] * len(my_broker._appliances)
            i = 0
            for appliance_id in my_broker._appliances:
                self.appliance_ids[i] = str(appliance_id)
                i+=1
            
class Nanogrid:
    
# Each Nanogrid has a Heartbeat function which will be called (by the Tkinter Root Window
# at periodic intervals - it can look after monitoring devices, changes in the environment etc
# In this example, it decrements the power_level variable and then makes sure that this
# new valule gets displayer by Tkinter

    def heartbeat(self):
        global power_level, displayed_power_level
        power_level=power_level -1
        displayed_power_level.set(str(power_level))            
        self.master.after(1000, self.heartbeat)  # after 100 msec, call heartbeat again

        # Update the listbox dynamically.
        listbox_data = broker_thread.appliance_ids
        for i in range(len(listbox_data)):
            if self.list.get(i) == listbox_data[i]:
                pass
            else:
                self.list.insert(i, listbox_data[i])

    def onselect(self, evt):
        # Note here that Tkinter passes an event object to onselect()
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        print('You selected item %d: "%s"' % (index, value))
        
        broker_thread.display = value
                
    def __init__(self, master=None):
        global power_level, displayed_power_level, displayed_time, broker_thread
        self.ok = 1      #used to shutdown the background threads when set to 0
        self.master = master
        broker_thread = BrokerThread()
        broker_thread.setDaemon(True)
        broker_thread.start()

        # start the broker
        _thread.start_new_thread(self.listener,())
        
        # put some graphical elements, two buttons and a displayed power level on window
        self.frame = Frame(master)
        self.frame.pack()

        self.button = Button(self.frame, text="QUIT", fg="red", command=self.shutdown)
        self.button.pack(side=LEFT)

        # Instantiate the list of time slots.
        labels = [Label()] * len(broker_thread.time_slots)
        label_list = []
        i=0

        # The time is displayed here.
        self.time_now = StringVar()
        self.time_now.set(broker_thread.broker_time)
        time_label = Label(self.frame,textvariable=broker_thread.broker_time, anchor="w",fg="blue",bg="white")
        time_label.pack(side=LEFT)

        self.display_text = broker_thread.coloured_text
        self.display_text.insert(INSERT, "")
        self.display_text.pack(side=LEFT)
        
        # Display the live data from the broker.
        self.broker_data = StringVar()
        self.broker_data.set(broker_thread.broker_data)
        data_label = Label(self.frame,textvariable=broker_thread.broker_data)
        #data_label.pack(side=LEFT)

        self.appliance_first_bid = StringVar()
        self.appliance_first_bid.set(broker_thread.appliance_first_bid_data)
        first_bid_label = Label(self.frame,textvariable=broker_thread.appliance_first_bid_data)
        first_bid_label.pack(side=LEFT)

        self.list = Listbox(self.frame, selectmode=EXTENDED)
        #self.list.bind('<<ListboxSelect>>', self.onselect)
        self.list.pack(fill=BOTH, expand=1)
        self.list.insert(ACTIVE, "Overview")
        self.list.update()
        
        displayed_power_level=StringVar()
        power_level =99
        displayed_power_level.set(str(power_level))
        #Label(self.frame, textvariable=displayed_power_level).pack(side=LEFT,padx=5)

        
        self.heartbeat()  # start the heartbeat function for the first time
        
    def shutdown(self):
        self.ok = 0
        self.master.after(100, root.destroy)

                
# This function is called in its own thread - it responds to incoming requests from
# other Nanogrids
    
    def listener (self):
        print ("listener started")
        # Create server
        server = SimpleXMLRPCServer((Constants.HOST, Constants.PORT),
                                    requestHandler=RequestHandler,
                                    allow_none=True)
        server.register_introspection_functions()
        server.register_instance(my_broker)
        # Run the server's main loop
        server.serve_forever()

if __name__ == "__main__":
    root = Tk()

    root.title("Broker")
    app = Nanogrid(root)
    root.mainloop()

