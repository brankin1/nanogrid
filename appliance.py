#!/usr/bin/python3
import os
import socket
import sys
import select
import datetime
from threading import Thread
import time
import traceback

class Constants:
    earliest_hour = 17
    earliest_minute = 0
    duration_hour = 3
    duration_minute = 0
    max_price = 100
    energy_required = 70
    max_load = 20
    reallocate_amount = 10/100
    tatonnement = True
    dutch_auction = False

class TimeSlot:
    def __init__(self, start_time, price, energy):
        self.start_time = start_time
        self.price = price
        self.energy = energy

class Appliance:
    def __init__(self, broker, appliance_config=None):
        self._broker = broker
        self._id = None
        # Which bidding session we are currently bidding for.
        self._bid_id = None
        # The last bidding session in which our bid was successful.
        self._last_successful_bid_id = None
        # The data we are bidding, containing the energy usage that we wish to receive for each time slot.
        self.energy_requested = []
        # List of TimeSlot. The known data that has been accepted by the server.
        self._accepted_time_slots = []
        self.clock = None
        self._last_bid_successful = False

        if appliance_config != None:
            Constants.earliest_hour = appliance_config.earliest_hour
            Constants.earliest_minute = appliance_config.earliest_minute
            Constants.duration_hour = appliance_config.duration_hour
            Constants.duration_minute = appliance_config.duration_minute
            Constants.max_price = int(appliance_config.max_price)
            Constants.energy_required = int(appliance_config.energy_required)
            Constants.max_load = int(appliance_config.max_load)
            Constants.reallocate_amount = appliance_config.reallocate_amount
            #Constants.tatonnement = appliance_config.tatonnement
            #Constants.dutch_auction = appliance_config.dutch_auction

    def join_broker(self):
        self.run()
        return self._id

    def run(self):
        self._id = self._broker.join()
        self.appliance_thread = Thread(target=self.appliance_thread)
        self.appliance_thread.start()
    
    def update_clock(self):
        self.clock = self._broker.get_clock()
        print(str(self.clock))

    def get_cheapest_indexes(self, acceptable_times):
        cheapest_index = []
        j = 0
        while j <  len(self._accepted_time_slots):
            for index, accepted_slot in enumerate(self._accepted_time_slots):
                if accepted_slot.start_time == acceptable_times[j]:
                    cheapest_index.append(index)
                    break
            else:
                index = -1
            j+=1
        return cheapest_index

    def get_most_expensive_indexes(self, acceptable_times):
        if acceptable_times != None:
            expensive_indexes = []
            rev_me = self.get_cheapest_indexes(acceptable_times)
            for i in reversed(rev_me):
                expensive_indexes.append(i)
            return expensive_indexes
        else:
            return None

    # The time_slots_to_change is the timeslots which will just be altered ... they will only be added to.
    def add_energies_to_cheapest_timeslots(self, time_slots_to_change, redistributable_energy, earliest_start_time_object, end_time_object,acceptable_times):
        # Find the index of the cheapest energies.
        cheapest_indexes = self.get_cheapest_indexes(acceptable_times)
        bid = []

        # Make the unedited list of energies to return
        for slot in time_slots_to_change:
            bid.append(slot[2])

        for i in range(len(bid)):
            if redistributable_energy == 0:
                break
            else:
                if time_slots_to_change[cheapest_indexes[i]][0] < earliest_start_time_object or \
                    time_slots_to_change[cheapest_indexes[i]][0] > end_time_object or \
                    time_slots_to_change[cheapest_indexes[i]][1] >= Constants.max_price or \
                    time_slots_to_change[cheapest_indexes[i]][2] >= Constants.max_load:
                    print("ADDENERGIESTOCHEAPEST Timeslot " + str(time_slots_to_change[cheapest_indexes[i]][0])+" not applicable! " + str(redistributable_energy) + " left to distribute!")
                # If the cheapest timeslot available is within the time
                else:
                    add_amount = min(Constants.max_load - bid[cheapest_indexes[i]], redistributable_energy)
                    print("\t add amount : "+ str(add_amount))
                    bid[cheapest_indexes[i]] += add_amount
                    redistributable_energy -= add_amount
        return bid

    def allocate_energy_to_cheapest_timeslots(self, time_slots, earliest_start_time_object, end_time_object, acceptable_times, redistributable_energy = Constants.energy_required):
        # Find the index of the cheapest energies.
        cheapest_index = self.get_cheapest_indexes(acceptable_times)
        # Create a new list of energies, and allocate the redistributable energies to the cheapest slots
        new_bid_slots = []

        # Set the new bid slots.
        for slot in time_slots:
            new_bid_slots.append(slot[2])

        j= 0
        for i in range(len(time_slots)):
            # If there is no energy left to distribute, 
            if redistributable_energy == 0:
                break
            else:
                # If the cheapest timeslot available is too expensive or has already allocated too much energy, move to the next cheapest
                if time_slots[cheapest_index[j]][0] < earliest_start_time_object or time_slots[cheapest_index[j]][0] > end_time_object or \
                    time_slots[cheapest_index[j]][1] >= Constants.max_price or time_slots[cheapest_index[j]][2] >= Constants.max_load:
                    print("ALLOCATEENERGIES Timeslot " + str(time_slots[cheapest_index[j]][0])+" not applicable! " + str(redistributable_energy) + " left to distribute!")
                    j+=1
                # If the cheapest timeslot available is within the time
                else:
                    # Find the amount to bid
                    if new_bid_slots[cheapest_index[j]] == Constants.max_load:
                        pass
                    elif new_bid_slots[cheapest_index[j]] + redistributable_energy > Constants.max_load \
                        and new_bid_slots[cheapest_index[j]] < Constants.max_load:
                        print("Timeslot " + str(time_slots[cheapest_index[j]][0])+" IS OKAY! " + str(redistributable_energy) + " left to distribute!")
                        # Add the new energies to the bid slot
                        energy_allocated_to_new_slot = Constants.max_load - new_bid_slots[cheapest_index[j]]
                        redistributable_energy -= energy_allocated_to_new_slot
                        new_bid_slots[cheapest_index[j]] = new_bid_slots[cheapest_index[j]] + energy_allocated_to_new_slot
                    elif new_bid_slots[cheapest_index[j]] + redistributable_energy <= Constants.max_load:
                        print("Timeslot " + str(time_slots[cheapest_index[j]][0])+" IS OKAY! " + str(redistributable_energy) + " left to distribute!")
                        new_bid_slots[cheapest_index[j]] = new_bid_slots[cheapest_index[j]] + redistributable_energy
                        redistributable_energy -= redistributable_energy
                        print("LAST MESSAGE. REDIS ENERGY REMAINING: "+ str(redistributable_energy))
                    j+=1
                    print(" ENERGY MOVED." + str(redistributable_energy) + " to " + str(cheapest_index[j-1]))

        return new_bid_slots

    def appliance_thread(self):
        """This is the main thread for the appliance. It runs on an infinite loop and updates the 
        appliance bidding and timeslot values."""
        try:
            session_id = 0
            while True:                
                do_bid = False
                print("Session id: " + str(session_id))
                # Get the current status of the timeslots and their associated session id.
                session_id, time_slots = self._broker.get_status(self._id)

                if session_id != self._bid_id:
                    # There is a new bidding session. Any existing bid is no longer valid.
                    self._last_bid_successful = False
                    self._bid_id = session_id
                    # Clear the last accepted time slots, and update them with the new values from this session.
                    self._accepted_time_slots = []
                    for time_slot in time_slots:
                        self._accepted_time_slots.append(TimeSlot(time_slot[0], time_slot[1], time_slot[2]))
                                
                if self._last_successful_bid_id != self._bid_id:
                    # We have not already had a successful bid in the current session.
                    #print("    last bid: success {}, bid_id {}".format(was_bid_successful, last_bid_id))
                    if session_id == self._bid_id:
                        if self._last_bid_successful == True:
                            self._last_successful_bid_id = self._bid_id
                        else:
                            do_bid = True
                    else:
                        do_bid = True
                                               
                if do_bid:
                    # We are going to create a bid using the timeslots from this bidding session.
                    # Set the time slots to bid on.
                    self.energy_requested = [0] * len(self._accepted_time_slots)
                    self.update_clock()
                    broker_clock = self.clock
                    now = datetime.datetime(int(broker_clock.value[:4]), int(broker_clock.value[4:6]), int(broker_clock.value[6:8]), int(broker_clock.value[9:11]), int(broker_clock.value[12:14]), int(broker_clock.value[15:17]))
                    earliest_start_time_object = datetime.datetime(now.year, now.month, now.day, Constants.earliest_hour, Constants.earliest_minute)
                    end_time_object = earliest_start_time_object + datetime.timedelta(0,0,0,1,Constants.duration_minute,Constants.duration_hour)
                    # Store the energy required
                    energy_required = Constants.energy_required
                    redistributable_energy = 0
                    i = 0

                    # Get a list of the acceptable times, starting with the cheapest first.
                    acceptable_times = []
                    # Sort the list to start with the cheapest energies first
                    cheapest_time_slots = []
                    cheapest_time_slots = sorted(time_slots, key=lambda time_slot: time_slot[1])
                    if cheapest_time_slots != None:
                        for ts in cheapest_time_slots:
                            acceptable_times.append(ts[0])

                    self.energy_requested = self.allocate_energy_to_cheapest_timeslots(time_slots, earliest_start_time_object, end_time_object, acceptable_times, Constants.energy_required)
                    
                    # If the prices have changed, and there is a limit on how much energy can be moved from the initial accepted bid,
                    # we must find out how much energy can be redistributed, and update self.energy_requested with these acceptable values
                    if Constants.tatonnement == True and len(self._accepted_time_slots) > 0:
                        # Reallocate some energies to the new time slot...
                        temp_time_slots = self._accepted_time_slots
                        # Find the amount of redistributable energy
                        if acceptable_times != None:
                            most_expensive_indexes = self.get_most_expensive_indexes(acceptable_times)
                            for time_slot in self._accepted_time_slots:
                                redistributable_energy += time_slot.energy * Constants.reallocate_amount

                            # Adjust the time slots for reallocation.
                            temp_redist = redistributable_energy
                            for i in range(len(most_expensive_indexes)):
                                if temp_redist == 0:
                                    break
                                else:
                                    deduct_amount = min(self._accepted_time_slots[most_expensive_indexes[i]].energy, temp_redist)
                                    print("\t deduct amount : "+ str(deduct_amount))
                                    temp_time_slots[most_expensive_indexes[i]].energy = self._accepted_time_slots[most_expensive_indexes[i]].energy - deduct_amount
                                    temp_redist -= deduct_amount

                        # Create a list of energies which can be used.
                        temp_list = []
                        for time_slot in temp_time_slots:
                            temp_list.append([time_slot.start_time, time_slot.price, time_slot.energy])

                    if redistributable_energy > 0:
                        self.energy_requested = self.add_energies_to_cheapest_timeslots(temp_list,redistributable_energy,earliest_start_time_object,end_time_object,acceptable_times)

                    print("Total energies bid : " + str(round(sum(self.energy_requested))))
                    self._last_bid_successful = self._broker.place_bid(self._id, self._bid_id, self.energy_requested)
                    print("Placed bid {}: {} : {}".format(self._bid_id, self.energy_requested, self._last_bid_successful))

                    if self._last_bid_successful:
                        last_bid_id = self._bid_id

                time.sleep(1)
        except:
            print("Error in timer thread: " + traceback.format_exc())
