if __name__ == "__main__":
    print("Hello")

    scheduling_horizon = []
    limit_on_energy_consumption = "20v"
    vector_for_each_appliance = [] #end goal

    total_energy_required_kwh = 60
    
class Appliance:
    def __init__(self, id, load, energy_needed):
        self.id = id
        self.load = load
        self.energy_needed = energy_needed
        self.scheduled_vector = []

class TimeSlot:
    def __init__(self, id, price, available_energy):
        self.id = id
        self.price = price
        self.available_energy = available_energy

def get_energy_vectors(appliances, scheduling_horizon, maximum_load, prices):
    """
    appliances is a list of appliances
    scheduling_horizon is the number of time slots for each vector
    maximum_load is the max load per timeslot
    prices is a list of yen
    """
    timeslots = []
    #Store the time slot, its allocated price and it's available energy
    for i in range(min(len(prices), scheduling_horizon)):
        timeslots.append(TimeSlot(i, prices[i], maximum_load))
    
    timeslots.sort(key=lambda timeslot: timeslot.price)
    for appliance in appliances:
        appliance.scheduled_vector[:] = []
        current_time_index = 0
        while current_time_index < len(timeslots) and appliance.energy_needed > 0:
            if appliance.load <= timeslots[current_time_index].available_energy:
                timeslots[current_time_index].available_energy -= appliance.load
                appliance.energy_needed -= appliance.load
                appliance.scheduled_vector.append(timeslots[current_time_index].id)
            current_time_index += 1

