#!/usr/bin/python3
import xml.etree.ElementTree as ET
import os
import time
import sys
import uuid
import datetime
from threading import Thread
import traceback
import queue
import configparser
import csv
import copy
from operator import add

# Boilerplate stuff.
class PriceTier:
    def __init__(self, boundary, price_adjust, type):
        self.boundary = 1 - int(boundary)/100
        self.type = type
        if type == "increase":
            self.price_adjust = 1 + int(price_adjust)/100
        elif type == "decrease":
            self.price_adjust = 1 - int(price_adjust)/100

class Constants():
    cf = configparser.ConfigParser()
    cf.read('config.cfg')
    log_appliances_file = "C:\\Users\\James\\Documents\\COLLEGE\\nanogrid\\logs\\" + \
        str(datetime.datetime.now()).replace(':', '').replace('.', '-')+ " appliances" ".csv"
    log_timeslots_file = "C:\\Users\\James\\Documents\\COLLEGE\\nanogrid\\logs\\" + \
        str(datetime.datetime.now()).replace(':', '').replace('.', '-')+ " timeslots" ".csv"
    # These are the types of auction.
    dutch_auction = False
    price_by_average = False
    tatonnement = False
    price_by_capacity = False
    # Update auction type from config.
    auction_type = cf['default']['auction_type']
    if auction_type == "tatonnement":
        tatonnement = True
    elif auction_type == "price_by_capacity":
        price_by_capacity = True
    # This will set the size of the windows, ie each time slot is 2 minutes
    time_slot_window_size = int(cf['default']['window_size'])
    hours_into_future = float(cf['default']['hours_into_future'])
    # This is the amount of windows that will be created for each hour
    time_slot_window_length = datetime.timedelta(0, 60*60*hours_into_future)
    initial_price = int(cf['server']['initial_price'])
    min_price = int(cf['server']['min_price'])
    equilibrium_boundary = int(cf['server']['equilibrium_boundary'])/100
    #adjust_price_percentage = int(cf['server']['adjust_price_percentage']) /100
    increase_price_percentage = int(cf['server']['increase_price_percentage']) /100
    decrease_price_percentage = int(cf['server']['decrease_price_percentage']) /100
    inertia_percentage = 1 - int(cf['server']['inertia_percentage']) /100
    inertia_boundary = 1 - (int(cf['server']['inertia_percentage']) + int(cf['server']['inertia_boundary'])) /100
    capacity_list = cf['server']['capacity_list']
    cl = capacity_list.split(',')
    capacity_list = []
    for cap in cl:
        if price_by_capacity == True:
            capacity_list.append(int(cap))
        else:
            capacity_list.append("-")
    HOST = cf['server']['host']
    PORT = int(cf['server']['port'])
    # Create the pricing tiers for when adjusting by capacity.
    increase_price_tiers = cf['tiered_pricing']['increase_tiers'].split(',')
    decrease_price_tiers = cf['tiered_pricing']['decrease_tiers'].split(',')

    pricing_tiers = []
    for tier in increase_price_tiers:
        pricing_tier = PriceTier(tier.split(':')[0], tier.split(':')[1], "increase")
        pricing_tiers.append(pricing_tier)
    for tier in decrease_price_tiers:
        pricing_tier = PriceTier(tier.split(':')[0], tier.split(':')[1], "decrease")
        pricing_tiers.append(pricing_tier)

def log_data(data):
    if type(data) == Bid:
        with open(Constants.log_appliances_file, 'a', newline='') as fp:
            a = csv.writer(fp, delimiter=',')
            data = [[data.appliance_id, data.bid_id, data.energies]]
            a.writerows(data)
    elif type(data) == TimeSlot:
        with open(Constants.log_timeslots_file, 'a', newline='') as fp:
            a = csv.writer(fp, delimiter=',')
            data = [[data.start_time.ctime(), data.price, sum(data.appliance_energies.values()) , data.appliance_energies]]
            a.writerows(data)
    elif type(data) == list:
        with open(Constants.log_timeslots_file, 'a', newline='') as fp:
            a = csv.writer(fp, delimiter=',')
            a.writerows(data)

class Appliance:
    def __init__(self, id):
        self.id = id
        # (was_successful, bid_id)
        self.bid_success = (False, None)
        self.meets_equilibrium = False
        
class TimeSlot:
    def __init__(self, start_time, price, capacity):
        self.start_time = start_time
        self.price = price
        # key: id, value: energy.
        self.appliance_energies = {}
        self.energy_demanded = 0
        self.capacity_available = capacity
        # Whether this is the first bidding session that this time slot is included in.
        self.is_new = True
        self.is_bidding_enabled = True

class Bid:
    def __init__(self, session_id, appliance_id, energies):
        self.session_id = session_id
        self.appliance_id = appliance_id
        # List of energy being bid for in each time slot.
        # As the session_id is checked, we know the time slots must match up with the server.
        self.energies = energies

# Register an instance; all the methods of the instance are
# published as XML-RPC methods.
class Broker():
    def __init__(self, broker_config = None):
        if broker_config != None:
            Constants.auction_type = broker_config.auction_type
            Constants.dutch_auction = broker_config.dutch_auction
            Constants.price_by_average = broker_config.price_by_average
            Constants.tatonnement = broker_config.tatonnement
            Constants.price_by_capacity = broker_config.price_by_capacity
            Constants.increase_price_percentage = broker_config.increase_price_percentage
            Constants.decrease_price_percentage = broker_config.decrease_price_percentage
            Constants.inertia_percentage = broker_config.inertia_percentage
            Constants.inertia_boundary = broker_config.inertia_boundary
            Constants.capacity_list = broker_config.capacity_list
            Constants.HOST = broker_config.HOST
            Constants.PORT = broker_config.PORT
            Constants.time_slot_window_size = broker_config.time_slot_window_size
            Constants.hours_into_future = broker_config.hours_into_future
            Constants.time_slot_window_length = broker_config.time_slot_window_length
            Constants.initial_price = broker_config.initial_price
            Constants.min_price = broker_config.min_price
            Constants.equilibrium_boundary = broker_config.equilibrium_boundary
            Constants.increase_price_tiers = broker_config.increase_price_tiers
            Constants.decrease_price_tiers = broker_config.decrease_price_tiers
            Constants.pricing_tiers = broker_config.pricing_tiers
        print("After assigning : " + str(Constants.hours_into_future))

        # The clock that the broker operates on
        self.clock = datetime.datetime(2015,1,1,0,0,0)
        self._appliances = {}
        self._time_slots = []
        self._times = self._create_times()
        self._auctions = []
        self.bid_history = []
        self._bidding_session_id = 1
        self.session_history = {}
        # Queue of Bid
        self._bids = queue.Queue()
        # Which appliances have made a successful bid already in the current bidding session.
        self._has_bid = {}
        self._at_equilibrium = False
        self._broker_thread = Thread(target=self.broker_thread, daemon=True)
        self._broker_thread.start()
        self._update_time_slots()
        # A list of the consumed timeslots. Used to keep track of energies consumed.
        self.consumed_timeslots = []
        self._total_energy_consumed = 0

    def _create_times(self):
        start_time = datetime.datetime(self.clock.year, self.clock.month, self.clock.day, self.clock.hour)
        result = []
        current_time = start_time
        time_delta = datetime.timedelta(0,0,0,0, Constants.time_slot_window_size)
        # Match up the prices with upcoming times.
        for _ in range(24*60//Constants.time_slot_window_size):
            result.append(current_time)
            current_time += time_delta
        return result

    def _check_equilibrium(self, equilibrium_boundary):
        # Check the equilibrium.
        total_energies_requested = []
        if Constants.equilibrium_boundary == 0 :
            for time_slot in self._time_slots:
                if self.clock < time_slot.start_time:
                    total_energies_requested.append(sum(time_slot.appliance_energies.values()))
            # If all of the energies have the same value, it is at an equilibrium.
            if all(x == total_energies_requested[0] for x in total_energies_requested):
                self._at_equilibrium = True
            else:
                self._at_equilibrium = False
        
        if Constants.equilibrium_boundary >= 0 and Constants.price_by_capacity:
            self._at_equilibrium = True
            # If there is an equilibrium boundary, check within those constraints too.
            for time_slot in self._time_slots:
                if sum(time_slot.appliance_energies.values()) > time_slot.capacity_available:
                    self._at_equilibrium = False
                elif sum(time_slot.appliance_energies.values()) < time_slot.capacity_available * (1 - Constants.equilibrium_boundary):
                    self._at_equilibrium = False

        print("Equilibrium status: \t\t" + str(self._at_equilibrium)) 
        return self._at_equilibrium

    def broker_thread(self):
        try:
            while True:
                for _ in range(6):
                    self._update_clock()
                    time.sleep(1)

                self._has_bid.clear()
                self._update_time_slots()
                if self._at_equilibrium == False:
                    self._bidding_session_id += 1
        except:
            print("Error in timer thread: " + traceback.format_exc())

    # Appliances join so that they are then stored in a table for future use.
    def join(self):
        id =  str(uuid.uuid4())
        self._appliances[id] = Appliance(id)
        return id

    def get_clock(self):
        return self.clock

    def _update_clock(self):
        self.clock += datetime.timedelta(0,1)
        print(str(self.clock))

    def _update_time_slots(self):
        """Add and remove time slots according to the current time, and set prices."""
        # Store the time slots associated with the bid ID at that point in time
        for time_slot in self._time_slots:
            log_data(time_slot)
            time_slot.is_new = False
                # Remove the time slot if it's start_time is later than now.
            if self.clock > time_slot.start_time:
                time_slot.is_bidding_enabled = False
        self._check_equilibrium(Constants.equilibrium_boundary)
        log_data([])

        # Set times.
        i = 0
        for time in self._times:
            if time > self.clock and time < self.clock + Constants.time_slot_window_length:
                # Check whether we need to create a new time slot.
                already_exists = False
                for time_slot in self._time_slots:
                    if time_slot.start_time == time:
                        already_exists = True
                if not already_exists:
                    self._time_slots.append(TimeSlot(time, Constants.initial_price, Constants.capacity_list[i]))
                    i+=1

        if self._bidding_session_id in self.session_history:
            pass
        else:
            self.session_history[self._bidding_session_id] = copy.deepcopy(self._time_slots)
            # Set prices.
            total_energy = 0
            for time_slot in self._time_slots:
                total_energy += sum(time_slot.appliance_energies.values())
            average_energy = total_energy / len(self._time_slots)

            # This sets the price based on the average price across all timeslots ...
            if Constants.price_by_average == True:
                for time_slot in self._time_slots:
                    if not time_slot.is_new:
                        if average_energy > 0:
                            time_slot.price = Constants.initial_price * sum(time_slot.appliance_energies.values()) / average_energy
                        else:
                            time_slot.price = Constants.initial_price

            # Tatonnement.
            if Constants.tatonnement == True:
                most_energy_used = 0
                # Get the max value of the energy used by one timeslot.
                for time_slot in self._time_slots:
                    if time_slot.appliance_energies.values():
                        most_energy_used = max(most_energy_used, sum(time_slot.appliance_energies.values()))

                if most_energy_used > 0:
                    for time_slot in self._time_slots:
                        if time_slot.is_new:
                            time_slot.price = Constants.initial_price
                        elif time_slot.start_time < self.clock:
                            pass
                        else:
                            if self._at_equilibrium == False:
                                # Stagnant
                                if sum(time_slot.appliance_energies.values()) <= (most_energy_used * Constants.inertia_percentage) and \
                                    sum(time_slot.appliance_energies.values()) >= (most_energy_used * Constants.inertia_boundary):
                                    print(str(most_energy_used) + " IS THE MOST ENERGY USED IN A TIMESLOT \n" + \
                                        str(sum(time_slot.appliance_energies.values())) + " IS THE FOR THIS TIMESLOT \n" + \
                                        str((most_energy_used * Constants.inertia_percentage)) + " IS THE inertia_percentage \n" + \
                                        str((most_energy_used * Constants.inertia_boundary)) + " IS THE inertia_boundary")
                                    time_slot.price = time_slot.price
                        
                                # Increase price if timeslot energy values are higher than inertia percentage
                                elif sum(time_slot.appliance_energies.values()) >= (most_energy_used * Constants.inertia_percentage):
                                    time_slot.price = max(Constants.min_price, time_slot.price * (1 + Constants.increase_price_percentage))
                            
                                # Decrease price if timeslot energy values are lower than inertia boundary
                                elif sum(time_slot.appliance_energies.values()) <= (most_energy_used * Constants.inertia_boundary):
                                    time_slot.price = max(Constants.min_price, time_slot.price * (1 - Constants.decrease_price_percentage))

                    for time_slot in self._time_slots:
                        print (str(time_slot.appliance_energies))

            # This is the "Dutch auction" method. The price drops on every round of bidding until it reaches the price minimum.
            if Constants.dutch_auction == True:
                for time_slot in self._time_slots:
                    if not time_slot.is_new:
                        if time_slot.price > Constants.min_price:
                            time_slot.price = max((time_slot.price-1) , Constants.min_price)

            if Constants.price_by_capacity == True:
                for time_slot in self._time_slots:
                    time_slot.energy_demanded = sum(time_slot.appliance_energies.values())
                    for tier in Constants.pricing_tiers:
                        if round(time_slot.energy_demanded) == time_slot.capacity_available:
                            print("Stagnant")
                            break
                        if round(time_slot.energy_demanded) > time_slot.capacity_available * tier.boundary and tier.type == "increase":
                            time_slot.price = time_slot.price * tier.price_adjust
                            print(str(round(time_slot.energy_demanded)) + str(time_slot.capacity_available))
                            print(tier.type + str(time_slot.start_time) + " increase")
                            break
                        elif round(time_slot.energy_demanded) < time_slot.capacity_available * tier.boundary and tier.type == "decrease":
                            time_slot.price = time_slot.price * tier.price_adjust
                            print(tier.type + str(time_slot.start_time))
                            break                
                    # Make sure the time slot isn't below the min price.
                    if time_slot.price < Constants.min_price:
                        time_slot.price = Constants.min_price

    
    def place_bid(self, appliance_id, session_id, energies):
        """
        energies: list will contain the amount of energy requested
        Determine whether bids are successful, update the time slot energies, and empty the bid queue
        """
        bid = Bid(session_id, appliance_id, energies)

        # Set the last successful bid id.
        # Update self._time_slots with energies from bids during the current bidding session - given by session_id.

        # If this appliance has not bid in this bidding session
        if bid.appliance_id not in self._has_bid:
            if bid.session_id == self._bidding_session_id:
                # Bid was successful.
                self._has_bid[bid.appliance_id] = True
                self._appliances[bid.appliance_id].bid_success = (True, bid.session_id)
                    
                # This is just for logging
                price_and_start_time = []
                for slot in self._time_slots:
                    price_and_start_time.append((slot.price, slot.start_time))
                bid_and_times = (bid, price_and_start_time)
                self.bid_history.append(bid_and_times)
                # End log.    

                # If the appliance has already bid in the time slots, remove the previous bid.
                for time_slot in self._time_slots:
                    if bid.appliance_id in time_slot.appliance_energies:
                        del time_slot.appliance_energies[bid.appliance_id]

                # The index of the first time slot which is enabled for bidding.
                first_time_slot_index = 0
                while not self._time_slots[first_time_slot_index].is_bidding_enabled:
                    first_time_slot_index += 1

                # Place the new bid for the appliance in to the time slots for later processing.
                for energy_index in range(len(bid.energies)):
                    self._time_slots[first_time_slot_index + energy_index].appliance_energies[bid.appliance_id] = bid.energies[energy_index]
            else:
                self._appliances[bid.appliance_id].bid_success = (False, bid.session_id)

        self._check_equilibrium(Constants.equilibrium_boundary)
        return self._appliances[bid.appliance_id].bid_success[0]

    def get_status(self, appliance_id):
        """Return (bidding_session_id, [(start_time, price, energy)], at_equilibrium)."""
        public_time_slots = []
        for time_slot in self._time_slots:
            if time_slot.is_bidding_enabled:
                energy = 0
                if appliance_id in time_slot.appliance_energies:
                    energy = time_slot.appliance_energies[appliance_id]
                public_time_slots.append((time_slot.start_time, time_slot.price, energy, time_slot.capacity_available))
        return (self._bidding_session_id, public_time_slots)

if __name__ == "__main__":
    pass
