import ipdb
from cvxopt import matrix
from cvxopt.modeling import variable
import xml.etree.ElementTree as ET

tree = ET.parse('prices.XML')
root = tree.getroot()

price_list = []

for pages in root.getchildren()[1].getchildren():
    for row in pages:
        date = row.getchildren()[1].text
        delivery_hour = row.getchildren()[2].text
        energy_price = row.getchildren()[5].text.lstrip()
        price_list.append(energy_price.lstrip())
        print(date + delivery_hour + " PRICE: " + energy_price)

#price_list = list(map(lambda x: float(x), strip_price.price_list))

print(len(price_list))

energy_scheduling_vector = variable()

scheduling_horizon = len(price_list)
limit_on_energy_consumption = 15 #This is the 'fuse box' number

