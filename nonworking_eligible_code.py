    def get_eligible_redistributable_energy_indexes(self, earliest_start_time, end_time):
        i = 0
        eligible_redistributable_energy_index = []
        most_expensive_timeslot_price = 0
        for time_slot in self._accepted_time_slots:
            most_expensive_timeslot_price = max(time_slot.price, most_expensive_timeslot_price)

        # Index the eligible timeslots
        for time_slot in self._accepted_time_slots:
            if time_slot.energy < Constants.max_load and \
                time_slot.start_time < end_time_object and \
                time_slot.start_time > earliest_start_time_object and \
                time_slot.price > most_expensive_timeslot_price:
                eligible_redistributable_energy_index.append(i)
            i+=1
        return eligible_redistributable_energy_index

    def get_eligible_timeslot_index(self,earliest_start_time,end_time):
        # Find the indexes of all timeslots which are within the start and end time.
        i = 0
        eligible_timeslot_index = []
        # Index the eligible timeslots
        for time_slot in self._accepted_time_slots:
            if time_slot.start_time < end_time and \
                time_slot.start_time > earliest_start_time:
                eligible_timeslot_index.append(i)
            i+=1
        return eligible_timeslot_index

    def get_eligible_index_as_cheapest_first(self, eligible_index):
        cheapest_eligible_timeslot_price = None
        for i in range(len(eligible_index)):
            cheapest_eligible_timeslot_price = min(cheapest_eligible_timeslot_price if cheapest_eligible_timeslot_price is not None else float('inf'), self._accepted_time_slots[i].price)
        # Sort the list to start with the cheapest energies first
        cheapest_eligible_time_slots = []
        cheapest_eligible_time_slot_index = []
        for i in range(len(eligible_index)):
            cheapest_eligible_time_slots.append(self._accepted_time_slots[i])
        cheapest_time_slots = sorted(cheapest_eligible_time_slots, key=lambda time_slot: time_slot[1])
        cheapest_eligible_time_slots = sorted(cheapest_eligible_time_slots, key=lambda time_slot: time_slot[1])

        for ts in cheapest_eligible_time_slots:
            cheapest_eligible_time_slot_index.append(self._accepted_time_slots.index(ts))
        return cheapest_eligible_time_slot_index

    def redistribute_energies(self, eligible_index):
        new_energy_bid_slots = []
        # This is the eligible indexes, cheap -> expensive
        cheapest_eligible_timeslot_index = self.get_eligible_index_as_cheapest_first(eligible_index)
        cheapest_eligible_timeslot_price = self._accepted_time_slots[cheapest_eligible_timeslot_index[0]]
        redistributable_energy = 0

        # Create a new list of energies, and allocate the redistributable energies to the cheapest slots
        # Should be left with a list of energies like [0,8,8,0] and redistributable_energies = 4
        new_energy_bid_slots = [0]*len(self._accepted_time_slots)
        if self._accepted_time_slots:
            i=0
            for slot in self._accepted_time_slots:
                if slot.price == cheapest_eligible_timeslot_price:
                    new_energy_bid_slots[i]=(slot.energy)
                elif slot.price < cheapest_eligible_timeslot_price:
                    new_energy_bid_slots[i] = (slot.energy*(1-Constants.reallocate_amount))
                    redistributable_energy += slot.energy*(Constants.reallocate_amount)
                i+=1

        # Distribute the redistributable energies in to the new_time_slots,
        # starting with the cheapest eligible timeslots first.
        for i in range(len(cheapest_eligible_timeslot_index)):
            if redistributable_energy == 0:
                break
            else:
                if new_energy_bid_slots[cheapest_eligible_timeslot_index[i]] >= Constants.max_load:
                    pass
                elif new_energy_bid_slots[cheapest_eligible_timeslot_index[i]] <= Constants.max_load:
                    redistribute_amount = Constants.max_load - new_energy_bid_slots[cheapest_eligible_timeslot_index[i]]
                    redistributable_energy -= redistribute_amount
                    new_energy_bid_slots[cheapest_eligible_timeslot_index[i]] += redistribute_amount
                    print("Just moved " + str(redistribute_amount) + " ... remaining: " + str(redistributable_energy))
        return new_energy_bid_slots
