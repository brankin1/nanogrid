#Script for obtaining the cloud cover over Dublin for a week from the windguru website. It used this and time data also fetched to calculate the Maximum Solar Irradiance available (without cloud cover) and the Actual Solar Irradiance available (with cloud cover). Maximum Solar Irradiance is calculated using the formula found at http://en.wikipedia.org/wiki/Insolation Q=S*cos(z) if cos(z) > 0 otherwise Q =0 (note the formula is simplified and different variables are used to avoid using greek characters) where Q is the solar irradiance, S is the solar constant adjusted for atmospheric losses (i.e. 1000W/m^2 rather than 1361W/m^2) and z is the sun zenith angle. z is calculated using the formula found at http://en.wikipedia.org/wiki/Solar_zenith_angle cos(z)=sin(l)sin(d)+cos(l)cos(d)cos(h) where l is the latitude, d is declination of the sun and h is the hours angle. The declination of the sun is calculated using the formula found at http://en.wikipedia.org/wiki/Declination_of_the_Sun#Declination_of_the_Sun_as_seen_from_Earth d = -23.44 * cos (360/365*(N+10)) where N is number of days since January 1st of the current year. The hour angle measures the how far the sun is from its daily peak and is calculated as the current hour minus twelve multiplied by 15 to convert it into an angle. The actual solar irradiance is then calculated by examining if the selecting the larger of the medium and low cloud cover. This is then converted to a value c between 0 and 1 (0 is no cloud cover and 1 is full cloud cover). The actual solar irradiance is then calculated using the following A =Q(1-c)+Q(c)/10. Q(c) is divided by ten as this an approximate measure of how much energy is lost due to clouds (THIS MAY CHANGE). 
import os
import time
import math
WeatherDataFile = '/project/nanogrid/WeatherData.txt'
#os.system("rm " + WeatherDataFile)
os.system('wget "http://www.windguru.cz/int/index.php?sc=4862" -O ~/WeatherData.txt')
exit=0
f =open(WeatherDataFile, 'r')
s = f.readline()
while s.find('LCDC')==-1:
    s = f.readline()
print(s)
i=s.find('"MCDC":')
j=s.find(',',i)
MCList=[]
if  s[i+8:j].isdigit():
    MCList.append(s[i+8:j])
else:
    MCList.append(0)
i=j+1
end=s.find(']',i)
while j<end:
    j=s.find(',',i)
    if j< end:        
        if  s[i:j].isdigit():
            MCList.append(s[i:j])
        else:
            MCList.append(0)
    if j> end:        
        if  s[i:j].isdigit():
            MCList.append(s[i:j-1])
        else:
            MCList.append(0)
    i=j+1
print("Medium Cloud Cover Percentage")
for x in range(len(MCList)):
    print(str(MCList[x])+','),
print()
i=s.find('"LCDC":')
j=s.find(',',i)
LCList=[]
if s[i:j].isdigit():
    LCList.append(s[i+8:j])
else:
    LCList.append(0)
i=j+1
end=s.find(']',i)
while j<end:
    j=s.find(',',i)
    if j< end:
        if s[i:j].isdigit():
            LCList.append(s[i:j])
        else:
            LCList.append(0)
    if j> end:        
        if s[i:j].isdigit():
            LCList.append(s[i:j-1])
        else:
            LCList.append(0)
    i=j+1
print("Low Cloud Cover Percentage")
for x in range(len(LCList)):
    print(str(LCList[x])+','),
print()
i=s.find('"hr_h":')
j=s.find(',',i)
HList=[]
HList.append(s[i+9:j-1])
i=j+1
end=s.find(']',i)
while j<end:
    j=s.find(',',i)
    if j< end:
        HList.append(s[i+1:j-1])
    if j> end:        
        HList.append(s[i+1:j-2])
    i=j+1
print("Hour")
for x in range(len(HList)):
    print(str(HList[x])+','),
print()
i=s.find('"hr_d":')
j=s.find(',',i)
DList=[]
DList.append(s[i+9:j-1])
i=j+1
end=s.find(']',i)
while j<end:
    j=s.find(',',i)
    if j< end:
        DList.append(s[i+1:j-1])
    if j> end:        
        DList.append(s[i+1:j-2])
    i=j+1
print("Day")
for x in range(len(DList)):
    print(str(DList[x])+','),
print()
TSList=[]
ASList=[]
for x in range(len(DList)):
    day=130+int(DList[x])#Number of Days from the Winter Equinox to start of May
    latitude=53.3478#Latitude of Dublin
    hour=int(HList[x])-12#Hour Angle based around noon==Zero
    DeclinationAngle=math.radians(-23.44)*math.cos(math.radians(0.98565*day))#Calculate Declination angle using -23.44 * cos (360/365*(N+10)) formula 
    ZenithAngle=math.sin(math.radians(latitude))*math.sin(DeclinationAngle)+math.cos(math.radians(latitude))*math.cos(DeclinationAngle)*math.cos(math.radians(hour*15))#Calculate Zenith angle using cos(z)=sin(l)sin(d)+cos(l)cos(d)cos(h)
    if ZenithAngle<0:#Account for if Cos(z)<0 Q=0
        TSList.append(0)
    else:
        TSList.append(1000*ZenithAngle)
    if int(LCList[x]) > int(MCList[x]):
         ASList.append((TSList[x]*(100-int(LCList[x]))/100)+(TSList[x]*int(LCList[x])/1000))#Percentage of time without cloud cover at full power and percentage of time with cloud cover at 10%
    else:
         ASList.append((TSList[x]*(100-int(MCList[x]))/100)+(TSList[x]*int(MCList[x])/1000))#Percentage of time without cloud cover at full power and percentage of time with cloud cover at 10%
print("Maximum Solar Irradiance (I.e. No Cloud Cover)")
for x in range(len(TSList)):
    print(str(TSList[x])+','),
print()
print("Actual Solar Irradiance Including Cloud Cover")
for x in range(len(ASList)):
    print(str(ASList[x])+','),
print()
batteryAvailable=[]
batteryPrices=[]
cTime=time.strftime("%H:%M")
cH=int(cTime[0:2],10)  
cM=int(cTime[3:5],10)
if(cM>=30):
    cM=1
else:
    cM=0
tH=int(HList[0],10)
sH=int((math.fabs(cH-tH)-1)/3)
print(str(sH))
print(str(cH))
temp=int(HList[sH],10)
print(str(temp)) 
sM=(math.fabs(temp-cH)*2)+cM
print(str(sM))
for x in range(0,48):
    batteryAvailable.append((ASList[sH]*(6-sM)/6+ASList[sH+1]*sM/6)*1.2)
    if batteryAvailable[len(batteryAvailable)-1]>0:
        batteryPrices.append(20)
    else:
        batteryPrices.append(500)
    sM=sM+1
    if sM==6:
        sM=0
        sH=sH+1
for x in range(len(batteryAvailable)):
    print("Current Battery Availability is "+str(batteryAvailable[x]))  
print("")
for x in range(len(batteryPrices)):
    print("Current Battery Price is "+str(batteryPrices[x]))  
print("")
