# N A N O G R I D
# definition of the base class of Nanogrid objects
# modified to work on Python3, January, 2015 [ changes to print, thread and xmlrpc ]
# Copyright: Donal O'Mahony, Trinity College, 2012, 2013

from tkinter import *
import _thread
import sys
import time
import socket
# Use XML Remote Procedure Calls to Communicate between Nanogrids
from xmlrpc.server import SimpleXMLRPCServer
import xmlrpc.client
import ipdb
import datetime
import configparser
 
HOST = "127.0.0.1"
PORT = 8000

#Set up a connection to the remote nanogrid on port 9000
REMOTE_RPC_HOST ="localhost"
REMOTE_RPC_PORT = 9000
nano = xmlrpc.client.ServerProxy('http://'+REMOTE_RPC_HOST+":"+str(REMOTE_RPC_PORT))
config = configparser.ConfigParser()

class Appliance:
    def __init__(self, total_energy_required, max_load, earliest_start, latest_start, load_type, contiguous):
        self.total_energy_required = total_energy_required
        self.max_load = max_load
        self.earliest_start = earliest_start
        self.latest_start = latest_start
        self.load_type = load_type
        self.contiguous = contiguous

class Nanogrid:
    
# Each Nanogrid has a Heartbeat function which will be called (by the Tkinter Root Window
# at periodic intervals - it can look after monitoring devices, changes in the environment etc
# In this example, it decrements the power_level variable and then makes sure that this
# new valule gets displayer by Tkinter

    def heartbeat(self):
        global power_level, displayed_power_level
        power_level=power_level -1
        displayed_power_level.set(str(power_level))            
        self.master.after(1000, self.heartbeat)  # after 100 msec, call heartbeat again
        
    def OnButtonClick(self):
        self.labelVariable.set( self.entryVariable.get()+" (You clicked the button)" )
        self.entry.focus_set()
        self.entry.selection_range(0, END)
        
    def OnPressEnter(self,event):
        self.labelVariable.set( self.entryVariable.get()+" (You pressed ENTER)" )
        self.entry.focus_set()
        self.entry.selection_range(0, END)        

    def __init__(self, master=None):
        global power_level, displayed_power_level
        self.ok = 1      #used to shutdown the background threads when set to 0
        self.master = master
        self.labelVariable = StringVar()

        # start the thread that listens for incoming requests from other Nanogrids
        _thread.start_new_thread(self.listener,())
        
        
        # put some graphical elements, two buttons and a displayed power level on window
        self.frame = Frame(master)
        self.frame.grid()


        #COLUMN 1
        #Print the decription of each of the menu items
        menu_description = ['Total energy required (W)','Max load(W)','Earliest start time(HH:MM)','Latest start time(HH:MM)','Load type','Contiguous']
        r = 0
        for option in menu_description:
            Label(text=option, relief=RIDGE,width=17).grid(row=r,column=1,sticky='EW')
            #Entry(bg=c, relief=SUNKEN,width=10).grid(row=r,column=1)
            r = r + 1
        
        #COLUMN 2
        self.energyRequiredVar = StringVar()
        self.energyRequired = Entry(textvariable=self.energyRequiredVar)
        self.energyRequired.grid(column=2,row=0,sticky='EW')

        self.energyRequired.bind("<Return>", self.OnPressEnter)
        self.energyRequiredVar.set(u"100")

        self.maxLoadVar = StringVar()
        self.maxLoadEntry = Entry(textvariable=self.maxLoadVar)
        self.maxLoadEntry.grid(column=2,row=1,sticky='EW')

        self.maxLoadEntry.bind("<Return>", self.OnPressEnter)
        self.maxLoadVar.set(u"40")

        self.earliestStartVar = StringVar()
        self.earliestStart = Entry(textvariable=self.earliestStartVar)
        self.earliestStart.grid(column=2,row=2,sticky='EW')

        self.earliestStart.bind("<Return>", self.OnPressEnter)
        self.earliestStartVar.set(u"12:00")
        
        self.latestStartVar = StringVar()
        self.latestStart = Entry(textvariable=self.latestStartVar)
        self.latestStart.grid(column=2,row=3,sticky='EW')

        self.latestStart.bind("<Return>", self.OnPressEnter)
        self.latestStartVar.set(u"13:00")
                
        self.loadType = StringVar()
        self.loadType.set("Select load type") # default value
        self.loadTypeWidget = OptionMenu(master, self.loadType, "Fixed", "Variable")
        self.loadTypeWidget.grid(column=2,row=4,sticky='EW')

        self.contiguous = StringVar()
        self.contiguous.set("Contiguous load?") # default value
        self.contiguousWidget = OptionMenu(master, self.contiguous, "Contiguous", "Non-contiguous")
        self.contiguousWidget.grid(column=2,row=5,sticky='EW')

        #COLUMN 3
        self.button = Button(master, text="QUIT", fg="red", command=self.shutdown)
        self.button.grid(column=3,row=0,sticky='EW')
        
        displayed_power_level=StringVar()
        power_level =99
        displayed_power_level.set(str(power_level))
        Label(master, textvariable=displayed_power_level).grid(column=3,row=1,sticky='EW')

        self.time_button = Button(master, text="Time?", command=self.get_time)
        self.time_button.grid(column=3,row=2,sticky='EW')

        self.bid_button = Button(master, text="Place bid", command=self.bid)
        self.bid_button.grid(column=3,row=3,sticky='EW')

        self.join_button = Button(master, text="Join", command=self.join)
        self.join_button.grid(column=3,row=4,sticky='EW')
        
#        self.click = Button(self.frame, text= "INPUT", command=self.OnButtonClick)
#        self.click.pack(column=3, row=5, sticky='EW')
       
        self.heartbeat()  # start the heartbeat function for the first time
       
        
    def shutdown(self):
        self.ok = 0
        self.master.after(100, root.destroy)
        
# method responding to the button push
    def say_hi(self):
        print ("hi there, everyone!")

    def get_time(self):
        #some sort of 'join' type message should be sent here to another client
        timenow = nano.get_time()
        # convert the ISO8601 string to a datetime object
        #converted = datetime.datetime.strptime(today.value, "%Y%m%dT%H:%M:%S")
        print("Time now: "  + timenow.value)

    def bid(self):
        app = Appliance(self.energyRequired.get(), self.maxLoadVar.get(), self.earliestStartVar.get(), self.latestStartVar.get(), self.loadType.get(), self.contiguous.get())
        print(app.max_load)
        pass
    
    def join(self):
        join_message = nano.join()
        print(join_message)

# This function is called in its own thread - it responds to incoming requests from
# other Nanogrids
    
    def listener (self):
         print ("listener started")
         # Create an RPC-server listening on localhost, port 8000
         server = SimpleXMLRPCServer((HOST, PORT))
    
         
         # Define a function that will respond to an incoming 'add' request
         def adder_function(x,y):
            print ("adder function invoked")
            return x + y
        
         # register that function with the RPC server
         server.register_function(adder_function, 'add')
         
         # Run the server's main loop
         server.serve_forever()


# mainline
#Create a Nanogrid object and kick it off into its eventloop

root = Tk()

root.title("Simple nanoGrid")
app = Nanogrid(root)
root.mainloop()

