#!/usr/bin/env python3
# N A N O G R I D
# definition of the base class of Nanogrid objects
# Copyright: Donal O'Mahony, Trinity College, 2012, 2013

try:
    from Tkinter import *
except ImportError:     
    from tkinter import *
try:    
    import thread 
except ImportError:
    import _thread as thread
import sys
import traceback
sys.path.append("/home/pi/quick2wire-python-api/")
import time
import re
import socket
# Use XML Remote Procedure Calls to Communicate between Nanogrids
try:
    from SimpleXMLRPCServer import SimpleXMLRPCServer
    from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
    import xmlrpclib
except ImportError:
    from xmlrpc.server import SimpleXMLRPCServer
    from xmlrpc.server import SimpleXMLRPCRequestHandler
    import xmlrpc.client




import RPi.GPIO as GPIO
import quick2wire.i2c as i2c

import os

import math


# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)# Restrict to a particular path. not sure what this is doing

    

# Connection class is used to encapsulate the data about a connection
  
class Connection:
    def __init__(self, port, connectionID, voltage, currentType):
        self.port=port
        self.connectionID=connectionID
        self.voltage=voltage
        self.currentType=currentType
        self.connectionEstablished=0
        self.minCurrent=0
        self.maxCurrent=0
        self.price=0

    def update(self, port, connectionID, voltage, currentType, connectionEstablished, minCurrent, maxCurrent, price):
        self.port=port
        self.connectionID=connectionID
        self.voltage=voltage
        self.currentType=currentType
        self.connectionEstablished=connectionEstablished
        self.minCurrent=minCurrent
        self.maxCurrent=maxCurrent
        self.price=price

#Class for Encapsulating data on the operation of a device for a single time period
class Operation:
    def __init__(self,jobNumber,operationNumber,startTime,endTime,idealStartTime,operationPower):
        self.jobNumber=jobNumber
        self.operationNumber=operationNumber
        self.startTime=startTime
        self.endTime=endTime
        self.operationPower=operationPower
        self.idealStartTime=idealStartTime
#Class for Encapsulating data on the operation of a device over the entire scheduling period
class Bid:
    def __init__(self,jobNumber,operationList,tardinessPenalty,electricityPricesList,electricitySupplier):
        self.jobNumber=jobNumber
        self.operationList=operationList
        self.tardinessPenalty=tardinessPenalty
        self.electricityPricesList=electricityPricesList
        self.electricitySupplier=electricitySupplier
        self.timePenalty=0
        self.electricityCost=0
        for Operation in operationList:
            self.timePenalty+=abs(Operation.idealStartTime-Operation.endTime)
            for x in range(Operation.startTime,Operation.endTime):
                self.electricityCost+=Operation.operationPower*electricityPricesList[x]/2000#Divide by 2000 /2 for half hour conversion /1000 for W to kW
        self.timePenalty=self.timePenalty*tardinessPenalty
        self.utility=-self.electricityCost-self.timePenalty
          
  
class Nanogrid:
    
# Each Nanogrid has a Heartbeat function which will be called (by the Tkinter Root Window
# at periodic intervals - it can look after monitoring devices, changes in the environment etc
# In this example, it decrements the power_level variable and then makes sure that this
# new valule gets displayer by Tkinter    
                              
        
    def __init__(self, IPAddress, ID):
        self.nanogridID=ID
        self.storage=1
        self.IPAddress=IPAddress
        self.in1=Connection("I1",self.nanogridID+"in1",12,"DC")
        self.in2=Connection("I2",self.nanogridID+"in2",12,"DC")
        self.out1=Connection("O1",self.nanogridID+"out1",12,"DC")

        self.batteryPrices=[]#Price List used in tatonnment process from buyer prospective 
        self.electricityPrices=[]#Price List used in tatonnment process from buyer prospective  
        self.batteryUsage=[]#Battery Usage used in tatonnment process
        self.electricityUsage=[]#Electricity Usage used in the tatonnment process
        self.totalElectricityUsage=0

        #Setup for Relays
        self.adc_address1 = 0x68#Setup information for reading Voltage and current
        self.adc_address2 = 0x69

        self.adc_channel1 = 0x98
        self.adc_channel2 = 0xB8
        self.adc_channel3 = 0xD8
        self.adc_channel4 = 0xF8       

        for line in open('/proc/cpuinfo').readlines():
                 m = re.match('(.*?)\s*:\s*(.*)', line)
                 if m:
                     (name, value) = (m.group(1), m.group(2))
                     if name == "Revision":
                         if value [-4:] in ('0002', '0003'):
                             self.i2c_bus = 0
                         else:
                             self.i2c_bus = 1
                         break 

        GPIO.cleanup() #Clean pins before starting                         
        
    
    def CONNECTSTORAGECHARGER(self, connectionType): #connectionType 0=floating, 1=input1, 2=input2
        if connectionType!=0 and connectionType!=1 and connectionType!=2:
            print ("Error. Invalid connectionType") 
            exit(1)

        if connectionType==0:
            try:                        
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(16, GPIO.OUT)
                GPIO.output(16,GPIO.LOW)           
                GPIO.setup(18, GPIO.OUT)
                GPIO.output(18,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==1:
            try:           
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(16, GPIO.OUT)
                GPIO.output(16,GPIO.HIGH)           
                GPIO.setup(18, GPIO.OUT)
                GPIO.output(18,GPIO.HIGH)
            except:
                print ("GPIO Error")
        if connectionType==2:
            try:           
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(16, GPIO.OUT)
                GPIO.output(16,GPIO.HIGH)           
                GPIO.setup(18, GPIO.OUT)
                GPIO.output(18,GPIO.LOW)
            except:
                print ("GPIO Error") 

    def CONNECTLOAD(self, connectionType): #connectionType 0=floating, 1=input1, 2=input2, 3=storage
        if connectionType!=0 and connectionType!=1 and connectionType!=2 and connectionType!=3:
            print ("Error. Invalid connectionType") 
            exit(1) 
        
        if connectionType==0:
            try:                        
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(26, GPIO.OUT)
                GPIO.output(26,GPIO.LOW)           
                GPIO.setup(24, GPIO.OUT)
                GPIO.output(24,GPIO.LOW)
                GPIO.setup(22, GPIO.OUT)
                GPIO.output(22,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==1:
            try:    
                GPIO.setmode(GPIO.BOARD)       
                GPIO.setup(24, GPIO.OUT)
                GPIO.output(24,GPIO.HIGH)
                time.sleep(1)
                GPIO.setup(22, GPIO.OUT)
                GPIO.output(22,GPIO.HIGH)
                time.sleep(1)
                GPIO.setup(26, GPIO.OUT)
                GPIO.output(26,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==2:
            try:  
                GPIO.setmode(GPIO.BOARD)         
                GPIO.setup(26, GPIO.OUT)
                GPIO.output(26,GPIO.HIGH)
                GPIO.setup(22, GPIO.OUT)
                GPIO.output(22,GPIO.LOW)
                GPIO.setup(24, GPIO.OUT)
                GPIO.output(24,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==3:
            try: 
                GPIO.setmode(GPIO.BOARD)          
                GPIO.setup(22, GPIO.OUT)
                GPIO.output(22,GPIO.HIGH)
                GPIO.setup(26, GPIO.OUT)
                GPIO.output(26,GPIO.LOW)
                GPIO.setup(24, GPIO.OUT)
                GPIO.output(24,GPIO.LOW)
            except:
                print ("GPIO Error") 

    def CONNECTOUTPUT(self,connectionType): #connectionType 0=floating, 1=input1, 2=input2, 3=storage
        if connectionType!=0 and connectionType!=1 and connectionType!=2 and connectionType!=3:
            print ("Error. Invalid connectionType") 
            exit(1)
        if connectionType==0:
            try:                        
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(19, GPIO.OUT)
                GPIO.output(19,GPIO.LOW)           
                GPIO.setup(21, GPIO.OUT)
                GPIO.output(21,GPIO.LOW)
                GPIO.setup(23, GPIO.OUT)
                GPIO.output(23,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==1:
            try:
                GPIO.setmode(GPIO.BOARD)           
                GPIO.setup(19, GPIO.OUT)
                GPIO.output(19,GPIO.HIGH)
                GPIO.setup(21, GPIO.OUT)
                GPIO.output(21,GPIO.HIGH)
                GPIO.setup(23, GPIO.OUT)
                GPIO.output(23,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==2:
            try:
                GPIO.setmode(GPIO.BOARD)           
                GPIO.setup(23, GPIO.OUT)
                GPIO.output(23,GPIO.HIGH)
                GPIO.setup(19, GPIO.OUT)
                GPIO.output(19,GPIO.LOW)
                GPIO.setup(21, GPIO.OUT)
                GPIO.output(21,GPIO.LOW)
            except:
                print ("GPIO Error")
        if connectionType==3:
            try: 
                GPIO.setmode(GPIO.BOARD)          
                GPIO.setup(19, GPIO.OUT)
                GPIO.output(19,GPIO.HIGH)
                GPIO.setup(23, GPIO.OUT)
                GPIO.output(23,GPIO.LOW)
                GPIO.setup(21, GPIO.OUT)
                GPIO.output(21,GPIO.LOW)
            except:
                print ("GPIO Error")  

    def CONNREQ(self, IPAddress, input):
        if input.connectionEstablished==1: # Check to see if connection is already established
            print ("Error. Input already connected")
        else:
            temp=input.connectionID
            s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
            result = s.messageCONNREQ(input.connectionID) # Communicate with other nanogrid            
            if result!=temp:
                input.update(input.port, result, input.voltage, input.currentType, 1,0,0,0)

    def POWERAUTH(self, IPAddress, connection, minCurrent, maxCurrent, price):
        if connection.connectionEstablished==0: # Check to see if a connection exists
            print ("Error. Connection has not been established")
        else:
            s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
            result = s.messagePOWERAUTH(connection.connectionID,connection.voltage,connection.currentType, minCurrent, maxCurrent, price) # Communicate with other nanogrid 
            if result != 0: # Check to see if price was acceptable. Should probaly return something to indicate various errors
                connection.update(connection.port, connection.connectionID, connection.voltage, connection.currentType, connection.connectionEstablished,minCurrent,maxCurrent,price)                     
                #print("I should probaly trigger a relay to connect the output to the load here unless I'm using it to charge my storage or forwarding it on")
                return result
            else:
                #print ("Further Negotiation could take place here")
                return 0  

    def GETELECTRICITYPRICES(self,IPAddress):    
        s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
        result = s.messageGETELECTRICITYPRICES()
        self.electricityPrices=result

    def GETBATTERYPRICES(self,IPAddress):
        s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
        result = s.messageGETBATTERYPRICES()
        self.batteryPrices=result 

    #Updates the usage pattern for the electricty supplier which causes the electricity supplier to operate its prices
    def TATONNMENT(self,IPAddress,iteration,usage):
        s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
        result = s.messageTATONNMENT(iteration,usage) 
        return result

    def TATONNMENTE(self,IPAddress,iteration,usage,totalUsage):  
        s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
        result = s.messageTATONNMENTE(iteration,usage,totalUsage) 
        return result

    def ISREADY(self,IPAddress):
        s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
        return s.messageISREADY()

    def GETCURRENTITERATION(self,IPAddress):
        s = xmlrpc.client.ServerProxy('http://'+IPAddress+':8000')
        return s.messageGETCURRENTITERATION()

    def getadcreading(self,address, channel):#Function for reading ADC pins
        with i2c.I2CMaster(self.i2c_bus) as bus: 
            bus.transaction(i2c.writing_bytes(address, channel))
            time.sleep(0.05)
            h, l, r = bus.transaction(i2c.reading(address,3))[0]
            time.sleep(0.05)
            h, l, r = bus.transaction(i2c.reading(address,3))[0]

            t = (h << 8) | l
            v = t * 0.000154
            if v < 5.5:
                return v
            else: # must be a floating input
                return 0.00 

# This function is called in its own thread - it responds to incoming requests from
# other Nanogrids

    

    def listener (self):

         print ("listener started")

         # Create an RPC-server listening on localhost, port 8000

         server = SimpleXMLRPCServer((self.IPAddress, 8000),requestHandler=RequestHandler)

         server.register_introspection_functions()

         

         # Define a function that will respond to an incoming 'add' request

         def messageCONNREQ(input):
            #Check to see if voltages and currentTypes match as well as making sure that the output is not already connected

            if self.out1.connectionEstablished==0:
                self.out1.update(self.out1.port, self.out1.connectionID+input, self.out1.voltage, self.out1.currentType, 1,0,0,0)

                return self.out1.connectionID+input


         # register that function with the RPC server

         server.register_function(messageCONNREQ, 'messageCONNREQ')

         def messagePOWERAUTH(connectionID, voltage, currentType, minCurrent, maxCurrent, price):
             if(self.out1.voltage!=voltage or self.out1.currentType!=currentType):#Assuming only out1 will have to change this to incorporate multiple connecitons
                 return 0 # indicate connection failed
             minimalPrice=0.3 #This should be updatable and a nanogrid variable but leave it like this until a pricing scheme has been set
             if self.out1.connectionEstablished==1 and price>minimalPrice:
                 self.out1.update(self.out1.port, self.out1.connectionID, self.out1.voltage, self.out1.currentType, self.out1.connectionEstablished,minCurrent,maxCurrent,price)                 
                 print("I trigger a relay here. I'd need another variable to indicate what input/storage to use if I had these.")
                 return price
             else:
                 print("price of connection could be adjusted here to enable renegotiation")
                 return 0

         server.register_function(messagePOWERAUTH, 'messagePOWERAUTH')

         

         # Run the server's main loop

         server.serve_forever()

class Display:
    #Function for checking the networking conditions and fixing them if their is a problem
    def checkNetwork(self):
        os.system("ifconfig > /home/pi/temp.txt")
        f = open('/home/pi/temp.txt','r')
        if f.read().find(self.loadGrid.IPAddress)!=-1:
            print("Networking Stable")

        else:
            while f.read().find(self.loadGrid.IPAddress)==-1:
                print("Networking Unstable. Restarting")
                os.system("sudo /etc/init.d/networking stop")
                os.system("sudo /etc/init.d/networking start")
                f.close()
                os.system("rm /home/pi/temp.txt")
                os.system("ifconfig > /home/pi/temp.txt")
                f = open('/home/pi/temp.txt','r')
        f.close()
        os.system("rm /home/pi/temp.txt")
    def heartbeat(self):
        self.checkNetwork()#Make sure the networking conditions are ok
        v3 = self.loadGrid.getadcreading(self.loadGrid.adc_address1, self.loadGrid.adc_channel4)
        v3=v3-2.21
        if (v3<0):
            v3=-v3
        i = v3*10
        print("Current: %f" % i)
        f = open('/home/pi/CurrentLog.csv','a')
        s=time.strftime("%d/%m/%Y %H:%M")+',r , '+str(i) +'\n'
        f.write(s)
        f.close()  
        self.master.after(60000, self.heartbeat)  # after 10000 msec, call heartbeat again
    def heartbeat2(self):
        self.checkNetwork()#Make sure the networking conditions are ok
        try:
            
            for k in range(1,100):
                print("Iteration Number "+str(k))
                self.loadGrid.GETBATTERYPRICES(self.renewableGrid.IPAddress)#Price List used in tatonnment process from buyer prospective   
                f = open('/home/pi/BatteryPrices.txt','a')
                f.write(str(k))
                for x in range(len(self.loadGrid.batteryPrices)):
                    f.write(",")
                    f.write(str(self.loadGrid.batteryPrices[x]))                    
                f.write("\n") 
                f.close()
                print("Battery Prices Received")                                      
                self.loadGrid.GETELECTRICITYPRICES(self.regularGrid.IPAddress)#Price List used in tatonnment process from buyer prospective
                f = open('/home/pi/ElectricityPrices.txt','a')
                f.write(str(k))
                for x in range(len(self.loadGrid.electricityPrices)):
                    f.write(",")
                    f.write(str(self.loadGrid.electricityPrices[x]))                    
                f.write("\n")
                f.close()
                print("Electricity Prices Received")
                self.loadGrid.electricityUsage=[]
                self.loadGrid.batteryUsage=[]
                self.loadGrid.totalElectricityUsage=0
                bidListJob=[]
                idealStartTime=0
                cTime=time.strftime("%H:%M")
                cH=int(cTime[0:2],10)  
                cM=int(cTime[3:5],10)
                if cM>=30:
                    cM=1
                else:
                    cM=0
                if cH>19:#If current time is before 19:00 set ideal start time as 19:00 otherwise set to be immediately
                    idealStartTime=0#Ideal Start time is 19:00. Lists are always updated to current time values so this has to be calculated each time the function is called
                else:
                    idealStartTime=(19-cH)*2-cM
                for i in range(len(self.loadGrid.electricityPrices)):
                    self.loadGrid.electricityUsage.append(0)
                    self.loadGrid.batteryUsage.append(0)
                for i in range(len(self.loadGrid.electricityPrices)-6):#Planning period is dependant on how much pricing information is available. -6 is length of job
                    opList =[]
                    op = Operation(1,1,i,i+6,idealStartTime,150)
                    opList.append(op)
                    b=Bid(1,opList,100,self.loadGrid.batteryPrices,1)#1 for renewable           
                    bidListJob.append(b)
                    b=Bid(1,opList,100,self.loadGrid.electricityPrices,0)#0 for grid
                    bidListJob.append(b)
                bidListJob.sort( key=lambda bid: bid.utility, reverse=True)#Get Job with smallest utility  
                for O in range(len(bidListJob[0].operationList)):
                    for x in range(bidListJob[0].operationList[O].startTime,bidListJob[0].operationList[O].endTime):
                        if bidListJob[0].electricitySupplier==0:
                            self.loadGrid.electricityUsage[x]+=bidListJob[0].operationList[O].operationPower
                            self.loadGrid.totalElectricityUsage+=bidListJob[0].operationList[O].operationPower
                for O in range(0,len(bidListJob[0].operationList)):
                    for x in range(bidListJob[0].operationList[O].startTime,bidListJob[0].operationList[O].endTime):
                        if bidListJob[0].electricitySupplier==1:
                            self.loadGrid.batteryUsage[x]+=bidListJob[0].operationList[O].operationPower
                f = open('/home/pi/BatteryUsage.txt','a')
                f.write(str(k))
                for x in range(len(self.loadGrid.batteryUsage)):
                    f.write(",")
                    f.write(str(self.loadGrid.batteryUsage[x]))                    
                f.write("\n") 
                f.close()
                print("Battery Usage Calculated")
                f = open('/home/pi/ElectricityUsage.txt','a')
                f.write(str(k))
                for x in range(len(self.loadGrid.electricityUsage)):
                    f.write(",")
                    f.write(str(self.loadGrid.electricityUsage[x]))                    
                f.write("\n") 
                f.close()
                print("Electricity Usage Calculated")
                while self.loadGrid.TATONNMENTE(self.regularGrid.IPAddress,k,self.loadGrid.electricityUsage,self.loadGrid.totalElectricityUsage)==0:
                    print("Iteration Mismatch")
                print("Electricity Tatonnment message sent")
                while self.loadGrid.TATONNMENT(self.renewableGrid.IPAddress,k,self.loadGrid.batteryUsage)==0:
                    print("Iteration Mismatch")
                print("Battery Tatonnment message sent")
                '''while (self.loadGrid.ISREADY(self.renewableGrid.IPAddress) != 1 or self.loadGrid.ISREADY(self.regularGrid.IPAddress) != 1):
                    print("Waiting for other updates")
                    print("Renewable Readiness "+str(self.loadGrid.ISREADY(self.renewableGrid.IPAddress)))
                    print("Grid Readiness "+str(self.loadGrid.ISREADY(self.regularGrid.IPAddress)))
                    time.sleep(1)'''
            print("Job Electricity Cost: "+str(bidListJob[0].electricityCost))
            print("Job Time Penalty: "+str(bidListJob[0].timePenalty)) 
            self.master.after(900000,self.heartbeat2)
        except:
            traceback.print_tb(sys.exc_info(),limit=1)
            self.master.after(900000,self.heartbeat2)

    def __init__(self, master=None):
        self.loadGrid = Nanogrid("192.168.1.111", "Load Grid")
        # start the thread that listens for incoming requests from other Nanogrids
        thread.start_new_thread(self.loadGrid.listener,())

        self.ok = 1      #used to shutdown the background threads when set to 0
        self.master = master                
        
        # put some graphical elements, two buttons and a displayed power level on window
        self.frame = Frame(master)
        self.frame.pack()

        self.button = Button(self.frame, text="QUIT", fg="red", command=self.shutdown)
        self.button.pack(side=LEFT)

        self.hi_there = Button(self.frame, text="Hello", command=self.say_hi)
        self.hi_there.pack(side=LEFT)

        self.renewableGrid = Nanogrid("192.168.1.112", "Renewable Grid")
        self.regularGrid = Nanogrid("192.168.1.102", "Regular Grid")

        self.loadGrid.CONNREQ(self.renewableGrid.IPAddress, self.loadGrid.in1) 
        self.loadGrid.CONNREQ(self.regularGrid.IPAddress, self.loadGrid.in2)

        self.heartbeat()  # start the heartbeat function for the first time
        self.heartbeat2()        

                
         

    def shutdown(self):
        GPIO.cleanup()
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(22, GPIO.OUT)
        GPIO.output(22,GPIO.LOW)
        GPIO.setup(24, GPIO.OUT)
        GPIO.output(24,GPIO.LOW)
        GPIO.setup(26, GPIO.OUT)
        GPIO.output(26,GPIO.LOW)
        GPIO.setup(23, GPIO.OUT)
        GPIO.output(23,GPIO.LOW)
        GPIO.setup(21, GPIO.OUT)
        GPIO.output(21,GPIO.LOW)
        GPIO.setup(19, GPIO.OUT)
        GPIO.output(19,GPIO.LOW)
        GPIO.setup(18, GPIO.OUT)
        GPIO.output(18,GPIO.LOW)
        GPIO.setup(16, GPIO.OUT)
        GPIO.output(16,GPIO.LOW)
        GPIO.cleanup()
        self.ok = 0
        self.master.after(100, root.destroy)
        
# method responding to the button push
    def say_hi(self):
        print ("hi there, everyone!")


# mainline
#Create a Nanogrid object and kick it off into its eventloop

root = Tk()

root.title("Simple nanoGrid")
app = Display(root)
root.mainloop()