import xmlrpc
from threading import Thread
from appliance import Appliance
#from broker import Broker
import configparser


class ApplianceConfig:
    earliest_hour = 15
    earliest_minute = 0
    max_price = 100
    energy_required = 100
    energy_consumption_limit = 20
    reallocate_amount = 10/100
    tatonnement = True
    dutch_auction = False

if __name__ == "__main__":
    
    cf = configparser.ConfigParser()
    cf.read('config.cfg')

    REMOTE_RPC_HOST = cf['server']['host']
    REMOTE_RPC_PORT = cf['server']['port']
    broker = xmlrpc.client.ServerProxy('http://'+REMOTE_RPC_HOST+":"+str(REMOTE_RPC_PORT), allow_none=True)
    broker2 = xmlrpc.client.ServerProxy('http://'+REMOTE_RPC_HOST+":"+str(REMOTE_RPC_PORT), allow_none=True)

    app_config = ApplianceConfig()

    app = Appliance(broker, app_config)
    app.run()

    while True:
        pass
    