# N A N O G R I D
# definition of the base class of Nanogrid objects
# modified to work on Python3, January, 2015 [ changes to print, thread and xmlrpc ]
# Copyright: Donal O'Mahony, Trinity College, 2012, 2013

from tkinter import *
import _thread
import sys
import time
import socket
# Use XML Remote Procedure Calls to Communicate between Nanogrids
from xmlrpc.server import SimpleXMLRPCServer
from time import gmtime, strftime
import time
import threading
import xmlrpc.client
import datetime

nodelist = []

class TimeThread(threading.Thread):
     def __init__(self):
         super(TimeThread, self).__init__()
         self.updated_time = StringVar()
         
     def run(self):
        while True:
            self.updated_time.set(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
            time.sleep(1)

class node:
    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port

# Register an instance; all the methods of the instance are
# published as XML-RPC methods
class MyFuncs:
    def test_function(self, test_string):
        return test_string + " is the string from the RPC server"
    
    def get_time(self):
        today = strftime("%Y-%m-%d %H:%M:%S", gmtime()) #datetime.datetime.today()
        return xmlrpc.client.DateTime(today)
    
    def join_node(self, node_host, node_port):
        result = "test"
        i = 0
        while i < len(nodelist):
            if len(nodelist) == 0:
                joined_node = node(node_host, node_port)
                nodelist.append(joined_node)
                result = "OK! Joined!"
                break
            elif nodelist[i].port == node_port and nodelist[i].host == node_port:
                result = "You were already a member!"
                break
            else:
                i+=1
                if i == len(nodelist):
                    joined_node = node(node_host, node_port)
                    nodelist.append(joined_node)
                    result = "OK! Joined!"
        return result
    
    
class Nanogrid:
    
# Each Nanogrid has a Heartbeat function which will be called (by the Tkinter Root Window
# at periodic intervals - it can look after monitoring devices, changes in the environment etc
# In this example, it decrements the power_level variable and then makes sure that this
# new valule gets displayer by Tkinter

    def heartbeat(self):
        global power_level, displayed_power_level
        power_level=power_level -1
        displayed_power_level.set(str(power_level))            
        self.master.after(1000, self.heartbeat)  # after 100 msec, call heartbeat again
        


    def __init__(self, master=None):
        global power_level, displayed_power_level
        self.ok = 1      #used to shutdown the background threads when set to 0
        self.master = master

        # start the thread which keeps a track of the time
        #Kick the time thread to keep an accurate time
        time_thread = TimeThread()
        time_thread.setDaemon(True)
        time_thread.start()

        # start the thread that listens for incoming requests from other Nanogrids
        _thread.start_new_thread(self.listener,())
        
        # put some graphical elements, two buttons and a displayed power level on window
        self.frame = Frame(master)
        self.frame.pack()

        self.button = Button(self.frame, text="QUIT", fg="red", command=self.shutdown)
        self.button.pack(side=LEFT)

        self.connected_nodes = Button(self.frame, text="Connected nodes", command=self.print_nodes)
        self.connected_nodes.pack(side=LEFT)

        #The time is displayed here.
        self.time_now = StringVar()
        self.time_now.set(time_thread.updated_time)
        time_label = Label(self.frame,textvariable=time_thread.updated_time, anchor="w",fg="blue",bg="white")
        time_label.pack(side=LEFT)
        #timeLabel.grid(column=0, row=3,sticky='EW')

        displayed_power_level=StringVar()

        power_level =99
        displayed_power_level.set(str(power_level))
        Label(self.frame, textvariable=displayed_power_level).pack(side=LEFT,padx=5)

        
        self.heartbeat()  # start the heartbeat function for the first time
        
    

        
        
    def shutdown(self):
        self.ok = 0
        self.master.after(100, root.destroy)
        
# method responding to the button push
    def say_hi(self):
        print ("hi there, everyone!")

    def print_nodes(self):
        for node in nodelist:
            print(str(node.host) + ":" + str(node.port))
                

# This function is called in its own thread - it responds to incoming requests from
# other Nanogrids
    
    def listener (self):
         print ("listener started")
         # Create an RPC-server listening on localhost, port 9000
         server = SimpleXMLRPCServer(("", 9000),
                        allow_none=True)
         server.register_instance(MyFuncs())
         # Run the server's main loop
         server.serve_forever()




# mainline
#Create a Nanogrid object and kick it off into its eventloop

root = Tk()

root.title("Simple nanoGrid")
app = Nanogrid(root)
root.mainloop()

