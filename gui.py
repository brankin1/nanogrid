#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import tkinter
from time import gmtime, strftime
import time
import threading

class TimeThread(threading.Thread):
     def __init__(self):
         super(TimeThread, self).__init__()
         self.updated_time = tkinter.StringVar()
         
     def run(self):
        while True:
            self.updated_time.set(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
            time.sleep(1)

class simpleapp_tk(tkinter.Tk):
    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        self.grid()

        self.entryVariable = tkinter.StringVar()
        self.entry = tkinter.Entry(self,textvariable=self.entryVariable)
        self.entry.grid(column=0,row=0,sticky='EW')
        self.entry.bind("<Return>", self.OnPressEnter)
        self.entryVariable.set(u"Enter text here.")

        button = tkinter.Button(self,text=u"Click me !",
                                command=self.OnButtonClick)
        button.grid(column=1,row=0)

        self.labelVariable = tkinter.StringVar()
        label = tkinter.Label(self,textvariable=self.labelVariable,
                              anchor="w",fg="white",bg="blue")
        label.grid(column=0,row=1,columnspan=2,sticky='EW')
        self.labelVariable.set(u"Hello !")
        
        #Kick the time thread to keep an accurate time
        thread1 = TimeThread()
        thread1.setDaemon(True)
        thread1.start()
        
        #The time is displayed here.
        self.timeNow = tkinter.StringVar()
        self.timeNow.set(thread1.updated_time)
        timeLabel = tkinter.Label(self,textvariable=thread1.updated_time, anchor="w",fg="blue",bg="white")
        timeLabel.grid(column=0, row=3,sticky='EW')

        self.grid_columnconfigure(0,weight=1)
        self.resizable(True,False)
        self.update()
        self.geometry(self.geometry())
        self.entry.focus_set()
        self.entry.selection_range(0, tkinter.END)
        
    def OnButtonClick(self):
        self.labelVariable.set( self.entryVariable.get()+" (You clicked the button)" )
        self.entry.focus_set()
        self.entry.selection_range(0, tkinter.END)

    def OnPressEnter(self,event):
        self.labelVariable.set( self.entryVariable.get()+" (You pressed ENTER)" )
        self.entry.focus_set()
        self.entry.selection_range(0, tkinter.END)

if __name__ == "__main__":
    app = simpleapp_tk(None)
    app.title('my application')
    app.mainloop()