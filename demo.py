import ipdb
from cvxopt import matrix
from cvxopt.modeling import variable
import xml.etree.ElementTree as ET

#Boilerplate stuff
max_load = 10
price_file = 'prices.XML'

class Appliance:
    def __init__(self, id, charging_load, energy_needed, energy_storage=40):
        self.id = id
        self.charging_load = charging_load
        self.energy_needed = energy_needed
        self.energy_storage = energy_storage
        self.scheduled_vector = [] #This is the positioning of the timeslots where the appliance will be turned "on"

class TimeSlot:
    def __init__(self, id, price, available_energy):
        self.id = id
        self.price = price
        self.available_energy = available_energy

def get_energy_vectors(appliances, scheduling_horizon, maximum_load, prices):
    """
    appliances is a list of appliances
    scheduling_horizon is the number of time slots for each vector
    maximum_load is the max load per timeslot
    prices is a list of yen
    """
    timeslots = []
    #Store the time slot, its allocated price and it's available energy
    for i in range(min(len(prices), scheduling_horizon)):
        timeslots.append(TimeSlot(i, prices[i], maximum_load))
    
    timeslots.sort(key=lambda timeslot: timeslot.price)
    
    #Iterate through the appliances, and designate them their scheduling vectors
    for appliance in appliances:
        appliance.scheduled_vector[:] = []
        current_time_index = 0
        while current_time_index < len(timeslots) and appliance.energy_needed > 0:
            if appliance.charging_load <= timeslots[current_time_index].available_energy:
                timeslots[current_time_index].available_energy -= appliance.charging_load
                appliance.energy_needed -= appliance.charging_load
                appliance.scheduled_vector.append(timeslots[current_time_index].id)
            current_time_index += 1

def get_price_list(file_location):
    #Get the prices from the prices.XML file
    tree = ET.parse(file_location)
    root = tree.getroot()
    price_list = []
    #The prices have been pulled from the semo website and need to be cleaned up
    for pages in root.getchildren()[1].getchildren():
        for row in pages:
            date = row.getchildren()[1].text
            delivery_hour = row.getchildren()[2].text
            energy_price = row.getchildren()[5].text.lstrip()
            price_list.append(energy_price.lstrip())
            print(date + delivery_hour + " PRICE: " + energy_price)
    #Convert the price strings to floats
    price_list = list(map(lambda x: float(x), price_list))
    return price_list

if __name__ == "__main__":
    print("Hello")

    #Get the list of prices
    price_list = get_price_list(price_file)

    #Create some appliances
    appliances = []
    appliances.append(Appliance("id1", 5, 40, 40))
    appliances.append(Appliance("id2", 5, 40, 40))
    appliances.append(Appliance("id3", 5, 40, 40))
    #This will adjust the scheduling vector for each appliance, taking in to account every time block
    get_energy_vectors(appliances, len(price_list), max_load, price_list)

    for appliance in appliances:
        #print(appliance.id + " scheduled vector " + str(appliance.scheduled_vector))
        total_cost = 0
        appliance_energy_storage = appliance.energy_storage
        #Print the total price to charge
        ipdb.set_trace()
        for scheduled_timeslot in appliance.scheduled_vector:
            #print(str(price_list[scheduled_timeslot]), end=" ")
            #Calculate the total price. The cost is proportionate to the energy it requires
            print("Remaining energy for " + appliance.id + " : " + str(appliance_energy_storage))
            if appliance_energy_storage > appliance.charging_load:
                total_cost += max_load/appliance.charging_load*price_list[scheduled_timeslot]
                appliance_energy_storage -= appliance.charging_load
            elif appliance_energy_storage == 0:
               break 
            else:
                total_cost += max_load/appliance_energy_storage*price_list[scheduled_timeslot]
                appliance_energy_storage = 0
            #print(str(total_cost)+ " SCHED_TIME " + str(scheduled_timeslot))
        print("")
        print("Total paid by " + appliance.id + " is " + str(total_cost))

