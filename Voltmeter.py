#Simple script to read the voltage from the ADC board. Can't read values higher than 5V so a voltage divider network is used. This is the reason for the conversion factor later. The conversion factor can be calculated as (r2/(r1+r2)) where r1 and r2 are the resistor values in the voltage divider network. There is usually a large variance in resistor though so it may be expedient to adjust it by trial and error. More information can be found at http://en.wikipedia.org/wiki/Voltage_divider
#!/usr/bin/env python3
# read abelectronics ADC Pi board inputs
# uses quick2wire from http://quick2wire.com/
# github: https://github.com/quick2wire/quick2wire-python-api
# Requries Python 3 
# I2C API depends on I2C support in the kernel
#
# This is a slightly modified version of the example code on http://www.abelectronics.co.uk/products/3/Raspberry-Pi/7/ADC-Pi---Raspberry-Pi-Analogue-to-Digital-converter
# See http://elinux.org/RPi_ADC_I2C_Python for full setup instructions
import sys
sys.path.append("/home/pi/quick2wire-python-api/")

import quick2wire.i2c as i2c

import re
import time

adc_address1 = 0x68
adc_address2 = 0x69

adc_channel1 = 0x98
adc_channel2 = 0xB8
adc_channel3 = 0xD8
adc_channel4 = 0xF8


for line in open('/proc/cpuinfo').readlines():
    m = re.match('(.*?)\s*:\s*(.*)', line)
    if m:
        (name, value) = (m.group(1), m.group(2))
        if name == "Revision":
            if value [-4:] in ('0002', '0003'):
                i2c_bus = 0
            else:
                i2c_bus = 1
            break

with i2c.I2CMaster(i2c_bus) as bus:

    def getadcreading(address, channel):
        bus.transaction(i2c.writing_bytes(address, channel))
        time.sleep(0.05)
        h, l, r = bus.transaction(i2c.reading(address,3))[0]
        time.sleep(0.05)
        h, l, r = bus.transaction(i2c.reading(address,3))[0]

        t = (h << 8) | l
        v = t * 0.000154
        if v < 5.5:
        	return v
        else: # must be a floating input
        	return 0.00

    while True:

        v3 = getadcreading(adc_address1, adc_channel1)
        print("1: %f" % v3)
        print("2: %f" % getadcreading(adc_address1, adc_channel2))
        print("3: %f" % getadcreading(adc_address1, adc_channel3))
        print("4: %f" % getadcreading(adc_address1, adc_channel4))

        print("5: %f" % getadcreading(adc_address2, adc_channel1))
        print("6: %f" % getadcreading(adc_address2, adc_channel2))
        print("7: %f" % getadcreading(adc_address2, adc_channel3))
        print("8: %f" % getadcreading(adc_address2, adc_channel4))
        v4=v3
        print("Test: %f" % v3)
        v4 =v4/float(0.0585)#Divide by conversion factor        
        print("Voltage: %f" % v4)
        f = open('/home/pi/VoltageLog.csv','a')
        s=time.strftime("%d/%m/%Y %H:%M")+', '+str(v4) +'\n'
        f.write(s)
        f.close()
        time.sleep(900)
